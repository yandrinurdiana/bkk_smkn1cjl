<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Tb_alumni List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('tb_alumni/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('tb_alumni/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('tb_alumni'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama</th>
		<th>Jenis Kelamin</th>
		<th>Ttl</th>
		<th>Alamat</th>
		<th>Tahun Lulus</th>
		<th>Email</th>
		<th>Kontak</th>
		<th>Pekerjaan</th>
		<th>Action</th>
            </tr><?php
            foreach ($tb_alumni_data as $tb_alumni)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $tb_alumni->nama ?></td>
			<td><?php echo $tb_alumni->jenis_kelamin ?></td>
			<td><?php echo $tb_alumni->ttl ?></td>
			<td><?php echo $tb_alumni->alamat ?></td>
			<td><?php echo $tb_alumni->tahun_lulus ?></td>
			<td><?php echo $tb_alumni->email ?></td>
			<td><?php echo $tb_alumni->kontak ?></td>
			<td><?php echo $tb_alumni->pekerjaan ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('tb_alumni/read/'.$tb_alumni->id_alumni),'Read'); 
				echo ' | '; 
				echo anchor(site_url('tb_alumni/update/'.$tb_alumni->id_alumni),'Update'); 
				echo ' | '; 
				echo anchor(site_url('tb_alumni/delete/'.$tb_alumni->id_alumni),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </body>
</html>