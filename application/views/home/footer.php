<!-- Start Footer bottom Area -->
<footer>
  <div id="contact" class="footer-area">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="footer-content">
            <div class="footer-head">
              <div class="footer-logo">
                <h2><span>Bursa Kerja Khusus</span><br> SMKN 1 Cijulang</h2>
              </div>
              <img src="<?php echo base_url(); ?>aset/img/logo_smk.png" alt=""/><br>

              <p><span>Alamat: </span>Jalan Mayoor Raswian Kondangjajar, Cijulang, Pangandaran 46394</p>
              
            </div>
          </div>
        </div>
        <!-- end single footer -->
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="footer-content">
            <div class="footer-head">
              <h4>Hubungi Kami</h4>
              <p>
                Jika ada pertanyaan ataupun keperluan bisa hubungi kami melalui alamat dibawah ini
              </p>

              <div class="footer-contacts">
                <p><span>Tel:</span>  (0265) 2640354</p>
                <p><span>Email:</span> kontak@smkncijulang.sch.id </p>
                <p><span>Web:</span><a href="http://www.smkncijulang.sch.id/"> SMK NEGERI 1 CIJULANG</a></p>
              </div>
              
              <div class="footer-icons">
                <ul>
                  <li>
                    <a href="https://www.facebook.com/pages/Smkn-1-Cijulang/881215961951337"><i class="fa fa-facebook"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                  </li>
                  <li>
                    <a href="https://www.instagram.com/anaksmkn1cijulang/"><i class="fa fa-instagram"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- end single footer -->

      </div>
    </div>
  </div>
  <div class="footer-area-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="copyright text-center">
            <p>
              &copy; 2019 Copyright. All Rights Reserved
            </p>
          </div>

        </div>
      </div>
    </div>
  </div>
</footer>

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript Libraries -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>aset/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>aset/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>aset/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>aset/lib/venobox/venobox.min.js"></script>
<script src="<?php echo base_url();?>aset/lib/knob/jquery.knob.js"></script>
<script src="<?php echo base_url();?>aset/lib/wow/wow.min.js"></script>
<script src="<?php echo base_url();?>aset/lib/parallax/parallax.js"></script>
<script src="<?php echo base_url();?>aset/lib/easing/easing.min.js"></script>
<script src="<?php echo base_url();?>aset/lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>aset/lib/appear/jquery.appear.js"></script>
<script src="<?php echo base_url();?>aset/lib/isotope/isotope.pkgd.min.js"></script>

<!-- Contact Form JavaScript File -->
<script src="<?php echo base_url(); ?>aset/contactform/contactform.js"></script>

<script src="<?php echo base_url();?>aset/js/main.js"></script>

<script>
  $('.owl-carousel').owlCarousel({
   nav:true,
   loop:true,
   margin:10,


   responsive:{
    0:{
      items:1
    },
    600:{
      items:3
    },
    1000:{
      items:5
    }
  }
})

</script>
</body>

</html>
