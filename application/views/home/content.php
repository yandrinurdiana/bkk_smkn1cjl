<!-- Start Slider Area -->
<div id="home" class="slider-area">
  <div class="bend niceties preview-2">
    <div id="ensign-nivoslider" class="slides">
     <img src="<?php echo base_url(); ?>aset/img/slider/smk.jpg" alt="" title="#slider-direction-1" />
     <img src="<?php echo base_url(); ?>aset/img/slider/hands.jpg" alt="" title="#slider-direction-2" />
     <img src="<?php echo base_url(); ?>aset/img/slider/slider.jpg" alt="" title="#slider-direction-3" />
   </div>

   <!-- direction 1 -->
   <div id="slider-direction-1" class="slider-direction slider-one">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content">
            <!-- layer 2 -->
            <div class="layer-1-2 wow fadeInDown" data-wow-duration="2s" data-wow-delay=".1s">
              <h1 class="title2">BKK SMKN 1 CIJULANG</h1>
            </div>
            <!-- layer 1 -->
            <div class="layer-1-1 hidden-xs wow slideInDown" data-wow-duration="2s" data-wow-delay=".2s">
              <h2 class="title1">Selamat Datang di Situs Resmi BKK SMKN 1 Cijulang, Temukan Pekerjaan Idaman Anda</h2>
            </div>
            <!-- layer 3 -->
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- direction 2 -->
  <div id="slider-direction-2" class="slider-direction slider-two">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content text-center">

            <!-- layer 2 -->
            <div class="layer-1-2 wow fadeInDown" data-wow-duration="2s" data-wow-delay=".1s">
              <h1 class="title2">Terpercaya</h1>
            </div>

            <!-- layer 1 -->
            <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
              <h2 class="title1">Bekerja Sama Dengan Banyak Perusahan Terpercaya </h2>
            </div>
            <!-- layer 3 -->

          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- direction 3 -->
  <div id="slider-direction-3" class="slider-direction slider-two">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content">

            <!-- layer 2 -->
            <div class="layer-1-2 wow slideInDown" data-wow-duration="2s" data-wow-delay=".1s">
              <h1 class="title2">Fleksibel</h1>
            </div>

            <!-- layer 1 -->
            <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
              <h2 class="title1">Lihat, Daftar, Dimanapun, Kapanpun</h2>
            </div>

            <!-- layer 3 -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- End Slider Area -->

<!-- Start About area -->
<div id="about" class="about-area area-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline text-center">
          <h2>Tentang BKK</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- single-well start-->
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="well-left">
          <div class="single-well">
            <a href="#">
              <img src="<?php echo base_url(); ?>aset/img/about/abt.jpeg" alt="">
            </a>
          </div>
        </div>
      </div>
      <!-- single-well end-->
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="well-middle">
          <div class="single-well">
            <a href="#">
              <h4 class="sec-head">Bursa Kerja Khusus</h4>
            </a>
            <p class="text-justify">
              Bursa Kerja Khusus (BKK) adalah sebuah lembaga yang dibentuk di Sekolah Menengah Kejuruan Negeri dan Swasta, sebagai unit pelaksana yang memberikan pelayanan dan informasi lowongan kerja, pelaksana pemasaran, penyaluran dan penempatan tenaga kerja, merupakan mitra Dinas Tenaga Kerja dan Transmigrasi.
            </p>
            <ul>
              <li>
                <i class="fa fa-check"></i> Sebagai wadah dalam mempertemukan tamatan dengan pencari kerja
              </li>
              <li>
                <i class="fa fa-check"></i> Memberikan layanan kepada tamatan sesuai dengan tugas dan fungsi
              </li>
              <li>
                <i class="fa fa-check"></i> Sebagai wadah dalam pelatihan tamatan
              </li>
              <li>
                <i class="fa fa-check"></i> Sebagai wadah untuk menanamkan jiwa wirausaha bagi tamatan
              </li>

            </ul>
          </div>
        </div>
      </div>
      <!-- End col-->
    </div>
  </div>
</div>
<!-- End About area -->


<!-- Start Informasi Terbaru Area -->
<div id="blog" class="blog-area">
  <div class="blog-inner area-padding bg-success">
    <div class="blog-overly"></div>
    <div class="container ">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Informasi Terbaru</h2>
          </div>
        </div>
      </div>

      <div class="row">
        <!-- Start Left Blog -->
        <?php foreach ($berita as $berita_item) { 
          if ($berita_item->status=='1') {
            ?>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="single-blog">
                <div class="single-blog-img">
                  <a href="blog.html">
                  
                    <img src="<?php echo base_url(); ?>image_berita/<?php echo $berita_item->image; ?>" class="card-img" alt="...">
                  </a>
                </div>
                <div class="blog-meta">
                  <span class="comments-type">
                    <i class="fa fa-user"></i> <?php echo $berita_item->author?>
                  </span>
                  <span class="date-type">
                    <i class="fa fa-calendar"></i><?php echo date('j F, Y', strtotime($berita_item->created_at)) ?>
                  </span>
                </div>
                <div class="blog-text">
                  <h4>
                    <a href="<?php echo base_url('welcome/readmore/'.$berita_item->id_berita); ?>"><?php echo $berita_item->judul ?></a>
                  </h4>
                  <p>
                    <?php echo substr($berita_item->isi, 0, 200); ?> ,...,.

                  </p>
                </div>
                <span>
                  <a href="<?php echo base_url('welcome/readmore/'.$berita_item->id_berita); ?>" class="ready-btn">Read more</a>
                </span>

              </div>

              <!-- Start single blog -->

            </div>
            <?php }} ?>

          </div>
          <div class="blog-pagination">
            <ul class="pagination">
              <li><?php 
              echo $this->pagination->create_links();
              ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End informasi -->

    <style>
    #owl-demo .item{
      background: transparent;
      padding: 30px 0px;
      margin: 10px;
      color: #FFF;
      -webkit-border-radius: 3px;
      -moz-border-radius: 3px;
      border-radius: 3px;
      text-align: center;
    }
  </style>
  <!-- Start Perusahaan Area -->
  <div id="perusahaan" class="our-team-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Mitra Perusahaan</h2> 
          </div>
        </div>
      </div>

      <div class="row">
        <div class="team-top">
          <div id="owl-demo" class="owl-carousel owl-theme">
            <?php foreach ($logo as $fl) {
             if ($fl->status=='1') { ?>

             <div class="item"><img style="width: 250px; height:250px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"   src="<?php echo base_url(); ?>logo_perusahaan/<?php echo $fl->gambar_logo; ?>" alt=""></div>
             <?php }} ?>
           </div>

         </div> <!-- End teamtop -->
       </div> <!-- End row -->
     </div><!-- End konterner -->
   </div><!-- End Team Area -->


   <!-- start pricing area -->
   <div id="lowongan" class="pricing-area area-padding bg-success">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Lowongan Tersedia</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <?php foreach ($loker as $lkr) { ?>
        <div class="card mb-3"  style="max-width: 720px;">
          <div class="row no-gutters">
            <div class="col-md-4">
             
              <img src="<?php echo base_url(); ?>image_loker/<?php echo $lkr->image; ?>" class="card-img" alt="...">
            </div>
            <div class="col-w-50">
              <div class="card-body">
                <h3 class="card-title"><?php echo $lkr->nama_perusahaan; ?></h3>
                <h5 class="card-text"><span>Posisi: </span><?php echo $lkr->posisi_kerja; ?></h5>
                <h6 class="card-text"><span>Kuota: </span><?php echo $lkr->kuota; ?></h6>
                <button pull-right class="btn btn-info" data-toggle="modal" data-target="#yourModal<?php echo $lkr->id_lk;?>">Lihat Detail</button>
                <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal<?php echo $lkr->id_lk;?>">Daftar</button>
              </div>
            </div>
          </div>
        </div><hr>

        <div class="modal fade" id="yourModal<?php echo $lkr->id_lk;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel"> <?php echo $lkr->nama_perusahaan; ?></h4>
              </div>
              <div class="modal-body">
                SPESIFIKASI DATA  <br>

                1.   Nama Perusahaan : &nbsp; <?php echo $lkr->nama_perusahaan; ?> <br>

                2. Poisi Kerja : &nbsp;<?php echo $lkr->posisi_kerja; ?><br>

                <?php echo $lkr->deskripsi; ?>


              </div>

            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModal<?php echo $lkr->id_lk;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $lkr->nama_perusahaan; ?> <br> posisi: <?php echo $lkr->posisi_kerja; ?><br><span></span></h4>
              </div>
              <div class="modal-body">
               <form method="POST" action="<?php echo base_url().'umum_reg/umum_register'; ?>" class="needs-validation" novalidate >
                <input type="hidden" name="id_lk" value="<?php echo $lkr->id_lk;?>">
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="nama" placeholder="Nama Lengkap" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="no_hp" placeholder="No Handphone" required>
                </div>
                <div class="form-group">
                  <input type="date" class="form-control" id="validationDefault01" name="tgl_lahir" placeholder="Tanggal Lahir" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="alamat" placeholder="Alamat Lengkap" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="email" placeholder="Email" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="tahun_lulus" placeholder="Tahun Lulus" required>
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label><br>
                  <input type="radio"  id="validationDefault01" name="jk" value="Laki-Laki" required>&nbsp;Laki-Laki&nbsp;&nbsp;
                  <input type="radio"  id="validationDefault01" name="jk" value="Laki-Laki" required>&nbsp;Perempuan
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="tinggi_badan" placeholder="Tinggi Badan" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="asal_sekolah" placeholder="Asal Sekolah" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="usia" placeholder="Usia" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="jurusan" placeholder="Jurusan" required>
                </div>

                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="nilai_un_matematika" placeholder="Nilai UN Matematika" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="validationDefault01" name="nilai_un_total" placeholder="Nilai UN Rata-Rata" required>
                </div>
                <div class="modal-footer">  
                 <button class="btn btn-success" type="submit"><i class="fa fa-send"></i> Kirim</button>
               </div>
             </form>
           </div>
         </div>
       </div>
     </div>

     
     <?php } ?>


   </div></div>
