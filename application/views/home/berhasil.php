<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>BKK - SMKN 1 CIJULANG</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>aset/img/faviconsmk.png" rel="icon">
  <link href="<?php echo base_url();?>aset/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url();?>aset/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url();?>aset/lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
  <link href="<?php echo base_url();?>aset/lib/owlcarousel/owl.carousel.css" rel="stylesheet">
  <link href="<?php echo base_url();?>aset/lib/owlcarousel/owl.transitions.css" rel="stylesheet">
  <link href="<?php echo base_url();?>aset/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>aset/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>aset/lib/venobox/venobox.css" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="<?php echo base_url();?>aset/css/nivo-slider-theme.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url();?>aset/css/style.css" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="<?php echo base_url();?>aset/css/responsive.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: eBusiness
    Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
  </head>

  <body data-spy="scroll" data-target="#navbar-example">

    <div id="preloader"></div>

    <header>
      <!-- header-area start -->
      <div id="sticker" class="header-area">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <!-- Navigation -->
              <nav class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <!-- Brand -->
                  <a class="navbar-brand page-scroll sticky-logo" href="<?php   echo base_url(''); ?>">
                    <!-- <h1><span>BKK</span> </h1>  -->

                    <!-- Uncomment below if you prefer to use an image logo -->
                    <img class="img" src="<?php   echo base_url(); ?>aset/img/logo1.png" alt="" title="BKK SMKN 1 CIJULANG"> 
                  </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                  <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                      <a class="page-scroll" href="<?php echo base_url('');?>">Home</a>
                    </li>
                    <li>
                      <a class="page-scroll" href="#about">Tentang</a>
                    </li>
                    
                    <li>
                      <a class="page-scroll" href="#blog">Informasi</a>
                    </li>

                    <li>
                      <a class="page-scroll" href="#perusahaan">Perusahaan</a>
                    </li>

                    <li>
                      <a class="page-scroll" href="#lowongan">Lowongan Pekerjaan</a>
                    </li>

                    <li>
                      <a href="<?php echo base_url('welcome/berkas'); ?>" >Download</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url('welcome/alumni'); ?>" >Alumni</a>
                    </li>

                    <li> 
                      <a href="<?php echo base_url('welcome/login'); ?>">login</a>
                    </li>
                  </ul>
                </div>
                <!-- navbar-collapse -->
              </nav>
              <!-- END: Navigation -->
            </div>
          </div>
        </div>
      </div>
      <!-- header-area end -->
    </header>
    <!-- header end -->

    <!-- Start Bottom Header -->
    <div class="header-bg page-area">
      <div class="home-overly"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="slider-content text-center">
              <div class="header-bottom">
                <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                  <h1 class="title2">Informasi</h1>
                </div>
                <div class="layer3 wow zoomIn" data-wow-duration="1s" data-wow-delay="1s">
                  <h2 class="title3"> BKK SMKN 1 CIJULANG</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Header -->
    <div class="blog-page area-padding">
      <div class="container">
        <div class="row">
          <!-- Start single blog -->
          <div class=" col-xs-12">
            <div class="row">
              
              <div class="col-md-12 col-sm-12 ">
                <!-- single-blog start -->
                <article class="blog-post-wrapper">
                  <div class="post-thumbnail">
                    
                  </div>
                  <div class="post-information">
                  	<h2>SELAMAT !!!</h2>
                    <h2>Pendaftaran anda berhasil terkirim, silahkan tunggu panggilan</h2>
                    
                   
                  </div>
                </article>
                <div class="clear"></div>

                <!-- single-blog end -->
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Blog Area -->
    <div class="clearfix"></div>

    <!-- Start Footer bottom Area -->
    <footer>
      <div id="contact" class="footer-area">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="footer-content">
                <div class="footer-head">
                  <div class="footer-logo">
                    <h2><span>Bursa Kerja Khusus</span><br> SMKN 1 Cijulang</h2>
                  </div>
                  <img src="<?php echo base_url(); ?>aset/img/logo_smk.png" alt=""/><br>

                  <p><span>Alamat: </span>Jalan Mayoor Raswian Kondangjajar, Cijulang, Pangandaran 46394</p>

                </div>
              </div>
            </div>
            <!-- end single footer -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="footer-content">
                <div class="footer-head">
                  <h4>Hubungi Kami</h4>
                  <p>
                    Jika ada pertanyaan ataupun keperluan bisa hubungi kami melalui alamat dibawah ini
                  </p>

                  <div class="footer-contacts">
                    <p><span>Tel:</span>  (0265) 2640354</p>
                    <p><span>Email:</span> kontak@smkncijulang.sch.id </p>
                    <p><span>Web:</span><a href="http://www.smkncijulang.sch.id/"> SMK NEGERI 1 CIJULANG</a></p>
                  </div>

                  <div class="footer-icons">
                    <ul>
                      <li>
                        <a href="https://www.facebook.com/pages/Smkn-1-Cijulang/881215961951337"><i class="fa fa-facebook"></i></a>
                      </li>
                      <li>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                      </li>
                      <li>
                        <a href="https://www.instagram.com/anaksmkn1cijulang/"><i class="fa fa-instagram"></i></a>
                      </li>
                      <li>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- end single footer -->

          </div>
        </div>
      </div>
      <div class="footer-area-bottom">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="copyright text-center">
                <p>
                  &copy; 2019 Copyright. All Rights Reserved
                </p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </footer>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="<?php echo base_url();?>aset/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>aset/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>aset/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>aset/lib/venobox/venobox.min.js"></script>
    <script src="<?php echo base_url();?>aset/lib/knob/jquery.knob.js"></script>
    <script src="<?php echo base_url();?>aset/lib/wow/wow.min.js"></script>
    <script src="<?php echo base_url();?>aset/lib/parallax/parallax.js"></script>
    <script src="<?php echo base_url();?>aset/lib/easing/easing.min.js"></script>
    <script src="<?php echo base_url();?>aset/lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>aset/lib/appear/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>aset/lib/isotope/isotope.pkgd.min.js"></script>

    <!-- Contact Form JavaScript File -->
    <script src="<?php echo base_url();?>aset/contactform/contactform.js"></script>

    <script src="<?php echo base_url();?>aset/js/main.js"></script>
  </body>

  </html>
