  <!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <title>BKK - SMKN 1 CIJULANG</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="<?php echo base_url(); ?>aset/img/faviconsmk.png" rel="icon">
    <link href="<?php echo base_url(); ?>aset/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="<?php echo base_url('aset/lib/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/lib/bootstrap/css/bootstrap.css" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Libraries CSS Files -->
    <link href="<?php echo base_url(); ?>aset/lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/lib/owlcarousel/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/lib/owlcarousel/owl.theme.default.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/lib/owlcarousel/owl.transitions.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/lib/venobox/venobox.css" rel="stylesheet">

    <!-- Nivo Slider Theme -->
    <link href="<?php echo base_url(); ?>aset/css/nivo-slider-theme.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="<?php echo base_url('aset/css/style.css'); ?>" rel="stylesheet">

    <!-- Responsive Stylesheet File -->
    <link href="<?php echo base_url(); ?>aset/css/responsive.css" rel="stylesheet">

    

    <!-- =======================================================
      Theme Name: BKK SMKN 1 CIJULANG
      Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
      Author: BootstrapMade.com
      License: https://bootstrapmade.com/license/
      ======================================================= -->
      <style>
      .img {
        max-width: 100%;
        height: auto;
      }
    </style>
  </head>

  <body data-spy="scroll" data-target="#navbar-example">

    <div id="preloader"></div>

    <header>
      <!-- header-area start -->
      <div id="sticker" class="header-area">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <!-- Navigation -->
              <nav class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <!-- Brand -->
                  <a class="navbar-brand page-scroll sticky-logo" href="<?php   echo base_url(''); ?>">
                    <!-- <h1><span>BKK</span> </h1>  -->

                    <!-- Uncomment below if you prefer to use an image logo -->
                    <img class="img" src="<?php   echo base_url(); ?>aset/img/logo1.png" alt="" title="BKK SMKN 1 CIJULANG"> 
                  </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                  <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                      <a class="page-scroll" href="#home">Home</a>
                    </li>
                    <li>
                      <a class="page-scroll" href="#about">Tentang</a>
                    </li>
                    
                    <li>
                      <a class="page-scroll" href="#blog">Informasi</a>
                    </li>

                    <li>
                      <a class="page-scroll" href="#perusahaan">Perusahaan</a>
                    </li>

                    <li>
                      <a class="page-scroll" href="#lowongan">Lowongan Pekerjaan</a>
                    </li>

                    <li>
                      <a href="<?php echo base_url('welcome/berkas'); ?>" >Download</a>
                    </li>
                    <li>
                      <a href="<?php echo base_url('welcome/alumni'); ?>" >Alumni</a>
                    </li>

                    <li> 
                      <a href="<?php echo base_url('welcome/login'); ?>">login</a>
                    </li>
                  </ul>
                </div>
                <!-- navbar-collapse -->
              </nav>
              <!-- END: Navigation -->
            </div>
          </div>
        </div>
      </div>
      <!-- header-area end -->
    </header>
  <!-- header end -->