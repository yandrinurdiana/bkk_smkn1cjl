<!-- <awalan yang baik untuk perusahaan> -->

<?php $this->load->view('perusahaan/v_head'); ?>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>BursaKhususKerja</b></span>
      </a>

      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <!-- Notifications: style can be found in dropdown.less -->
            <!-- Tasks: style can be found in dropdown.less -->
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>asetad/dist/img/profile.png" class="user-image" alt="User Image">
                <span class="hidden-xs">Welcome</span> 
                
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>asetad/dist/img/profile.png" class="img-circle" alt="User Image">
                  <p>
                    Wa Haji Amuh - Pengusaha Tempe
                    <small>Member since Nov. 2012</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Setting</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>" class="btn btn-danger btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control  untuk Sidebar Toggle Button sebelah kanan -->
            
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    
    <!-- search form -->
    
    <!-- /.search form -->
    <!-- untuk sidebar menu: : style can be found in sidebar.less -->
    <?php $this->load->view('perusahaan/v_sidebar'); ?>
    <!-- Content Wrapper. Contains page content -->  
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $judul; ?>
          <small>Version 1.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('perusahaan'); ?>"><i class="fa fa-home"></i>  <?php echo $judul; ?></a></li>
          <li class="active"> <?php echo $sub_judul; ?></li>
        </ol>
      </section>

      <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon green"></i>
        <strong>Sistem Aplikasi Bursa Khusus Kerja || SMKN 1 CIJULANG<small> (v1.0)</small></strong>
      </div>

      <!-- awal konten tengah -->
      <section class="content">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Info Lowongan Pekerjaan</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
            <?php echo $this->session->flashdata('msg'); ?></p>
             
              <?php echo form_open_multipart('page_perushaan_controller/upload'); ?>
                <div class="box-body">
                  <div class="form-group">
                    <label for="text">Judul Loker</label>
                      <input type="hidden" name="id_perusahaan" value="<?php echo $this->session->userdata('ses_id');?>">
                    <input type="text" class="form-control" id="text" placeholder="Nama Perusahaan" name="nama_perusahaan">
                  </div>
                  <div class="form-group">
                    <label for="text">Posisi</label>
                    <input type="text" class="form-control" id="text" placeholder="Posisi Bekerja" name="posisi_kerja">
                  </div>
                  <div class="form-group">
                    <label for="text">Kontak</label>
                    <input type="text" class="form-control" id="text" placeholder="Kontak" name="kontak_person">
                  </div>
                  <div class="form-group">
                    <label for="text">Tanggal Berakhir</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="date" class="form-control pull-right" id="datepicker" name="tanggal_kadaluarsa">
                    </div>
                    <div class="form-group">
                      <label>Deskripsi</label>
                      <textarea class="form-control" rows="3" placeholder="Deskripsi" name="deskripsi"></textarea>
                    </div>
                    <div class="form-group">
                    <label for="text">Kuota Loker</label>
                    <input type="text" class="form-control" id="text" placeholder="kuota loker" name="kuota_loker">
                  </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile" name="image" >
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="reset" class="btn btn-flat">Reset</button>
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                  </div>
                  <?php echo form_close(); ?>
            
                
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer -->
          </section>

        </div>

<!-- Add the sidebar's background. This div must be placed
 immediately after the control sidebar -->
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<?php  $this->load->view('perusahaan/v_script'); ?>
<script src="<?php echo base_url();?>asetad/tinymce/tinymce.min.js"></script>
 <script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        height: 300,
        theme: 'modern',
        plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insert file undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        file_picker_types: 'file image media',
        image_advtab: true
    });
</script>
</body>
</html>
