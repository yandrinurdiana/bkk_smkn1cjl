<!-- <awalan yang baik untuk perusahaan> -->

<?php $this->load->view('perusahaan/v_head'); ?>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>BursaKhususKerja</b></span>
      </a>

      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <!-- Notifications: style can be found in dropdown.less -->
            <!-- Tasks: style can be found in dropdown.less -->
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>asetad/dist/img/profile.png" class="user-image" alt="User Image">
                <span class="hidden-xs">Welcome</span> 
                
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>asetad/dist/img/profile.png" class="img-circle" alt="User Image">
                  <p>
                    Wa Haji Amuh - Pengusaha Tempe
                    <small>Member since Nov. 2012</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Setting</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>" class="btn btn-danger btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control  untuk Sidebar Toggle Button sebelah kanan -->
            
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    
    <!-- search form -->
    
    <!-- /.search form -->
    <!-- untuk sidebar menu: : style can be found in sidebar.less -->
    <?php $this->load->view('perusahaan/v_sidebar'); ?>
    <!-- Content Wrapper. Contains page content -->  
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $judul; ?>
          <small>Version 1.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('perusahaan'); ?>"><i class="fa fa-home"></i>  <?php echo $judul; ?></a></li>
          <li class="active"> <?php echo $sub_judul; ?></li>
        </ol>
      </section>

      <!-- awal konten tengah -->
      <section class="content">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Gaji Pegawai</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
            <?php foreach ($siswa as $u) {
    ?>
              <form role="form" action="<?php echo base_url().'page_perushaan_controller/update'; ?>" method="post">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="hidden" name="id_gaji" value="<?php echo $u->id_gaji; ?>">
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Siswa" name="nama_siswa" value="<?php echo $u->nama_siswa; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Asal Sekolah</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Sekolah" name="nama_sekolah" value="<?php echo $u->nama_sekolah; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Besaran Gaji</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Gaji Pertama" name="jumlah_gaji" value="<?php echo $u->jml_gaji; ?>">
                  </div>
                  <div class="form-group">
                    <label>Penilaian</label>
                    <textarea class="form-control" rows="3" placeholder="Kinerja pegawai" name="penilaian"> <?php echo $u->penilaian; ?> </textarea>
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="reset" class="btn btn-flat">Reset</button>
                  <button type="submit" name="submit" value="" class="btn btn-primary pull-right">Simpan</button>
                </div>
              </form>
              <?php
} ?>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->

          <!-- /.box-footer -->
        </section>

      </div>
      
<!-- Add the sidebar's background. This div must be placed
 immediately after the control sidebar -->
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<?php  $this->load->view('perusahaan/v_script'); ?>

</body>
</html>
