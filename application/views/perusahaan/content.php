<section class="content">
      <!-- Info boxes -->
      <!-- Small boxes (Stat box) -->
      <div class="row">

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <p>Jumlah Alumni</p>
              <h3>150234</h3>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <p>Perusahaan</p>
              <h3>150234</h3>
            </div>
            <div class="icon">
              <i class="fa fa-bank"></i>
            </div>
          </div>
        </div>
      </div> 
      <div class="callout callout-danger">
          <h4><i class="fa fa-bullhorn"></i> Tip!</h4>
          <p>pemberitahuan.</p>
        </div>
    </section>