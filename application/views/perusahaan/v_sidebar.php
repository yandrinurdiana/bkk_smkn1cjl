  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>asetad/dist/img/profile.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>PT.Aslina Lur</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGASI SISTEM</li>
        <li class="active">
          <a href="<?php echo base_url('page_perushaan_controller/index'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard Perusahaan</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tv"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('page_perushaan_controller/data_gaji'); ?>"><i class="fa fa-circle-o"></i> Data Gaji</a></li>
            <li><a href="<?php echo base_url('page_perushaan_controller/data_loker'); ?>"><i class="fa fa-circle-o"></i> Data Lowongan Kerja</a></li>

          </ul>
        </li>
        <li>
          <a href="<?php echo base_url('perusahaan/profil'); ?>">
            <i class="fa fa-cog"></i> <span>Setting</span>
          </a>
        </li>
        
        <li>
          <a href="<?php echo base_url(); ?>">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
      </li>        
    </ul>

  </section>
  <!-- /.sidebar -->
</aside>