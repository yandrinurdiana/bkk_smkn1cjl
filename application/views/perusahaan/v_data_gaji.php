<!-- <awalan yang baik untuk perusahaan> -->

<?php $this->load->view('perusahaan/v_head'); ?>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>BursaKhususKerja</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>asetad/dist/img/profile.png" class="user-image" alt="User Image">
                <span class="hidden-xs">Welcome <?php echo $this->session->userdata('ses_nama'); ?></span> 
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>asetad/dist/img/profile.png" class="img-circle" alt="User Image">
                  <p>
                    Wa Haji Amuh - Pengusaha Tempe
                    <small>Member since Nov. 2012</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Setting</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>" class="btn btn-danger btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>              
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <!-- untuk sidebar menu: : style can be found in sidebar.less -->
    <?php $this->load->view('perusahaan/v_sidebar'); ?>
    <!-- Content Wrapper. Contains page content -->  
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $judul; ?>
          <small>Version 1.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('perusahaan'); ?>"><i class="fa fa-home"></i>  <?php echo $judul; ?></a></li>
          <li class="active"> <?php echo $sub_judul; ?></li>
        </ol>
      </section>

      <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon green"></i>
        <strong>Sistem Aplikasi Bursa Khusus Kerja || SMKN 1 CIJULANG<small> (v1.0)</small></strong>
      </div>
      <!-- awal konten tengah -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Data Gaji Alumni SMK</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <a href="<?php echo site_url('page_perushaan_controller/input_gaji'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</a>
                  <thead>
                    <tr>
                      <th>no</th>
                      <th class="text-center">Nama</th>
                      <th>Asal Sekolah</th>
                      <th>Besaran Gaji</th>
                      <th>Penilaian</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
      $no = 1;
      foreach ($siswa as $u) {
         if ($this->session->userdata('ses_id')==$u->id_perusahaan) {
          ?>
                    <tr>
                      <td>1<?php echo $no++; ?></td>
                      <td><?php echo $u->nama_siswa; ?></td>
                      <td><?php echo $u->nama_sekolah; ?></td>
                      <td><?php echo $u->jml_gaji; ?></td>
                      <td><?php echo $u->penilaian; ?></td>
                      <td class="text-center">
                        <a href="<?php echo site_url('page_perushaan_controller/edit/'.$u->id_gaji); ?>" class="btn btn-success " ><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo site_url('page_perushaan_controller/hapus/'.$u->id_gaji); ?>" class="btn btn-danger " ><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php 
                  }
      } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      

    </div><!--akhir conten-->

<!-- Add the sidebar's background. This div must be placed
 immediately after the control sidebar -->
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<?php  $this->load->view('perusahaan/v_script'); ?>

</body>
</html>
