<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('admin/sidebar'); ?>



      <!-- awal konten tengah -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Data Loker</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  
                  <thead>
                    <tr>
                      <th>no</th>
                      <th class="text-center">Judul Loker</th>
                      <th>Posisi</th>
                      <th>Kontak</th>
                      <th>Tanggal Berakhir</th>
                      <th>Deskripsi</th>
                      <th>Kuota Loker</th>
                      <th>Gambar</th>
                      <th>Perusahaan</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
      $no = 1;
      foreach ($siswa as $u) {
          
          ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $u->nama_perusahaan; ?></td>
                      <td><?php echo $u->posisi_kerja; ?></td>
                      <td><?php echo $u->kontak_person; ?></td>
                      <td><?php echo $u->tanggal_kadaluarsa; ?></td>
                      <td><?php echo substr($u->deskripsi, 0, 200); ?> ....</td>
                      <td><?php echo $u->kuota; ?></td>
                      <td><img src="<?php echo base_url(); ?>image_loker/<?php echo $u->image; ?>" width="100" height="100"></td>
                      <td><?php echo $u->perusahaan; ?></td>
                      <td class="text-center">
                        <a href="<?php echo site_url('page_perushaan_controller/edit_loker_admin/'.$u->id_lk); ?>" class="btn btn-success " ><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo site_url('page_perushaan_controller/hapus_loker_admin/'.$u->id_lk); ?>" class="btn btn-danger " ><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php }
                     ?>
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>

    </div>

<!-- Add the sidebar's background. This div must be placed
 immediately after the control sidebar -->
 <div class="control-sidebar-bg"></div>
</div>


<?php $this->load->view('admin/footer'); ?>