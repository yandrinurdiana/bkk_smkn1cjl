

<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('admin/sidebar'); ?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box box-primary">
					 <?php echo $error;?>
         			 <?php echo $message;?><br>
					<?php echo form_open_multipart('File_controller/do_upload_logo');?>
						<div class="box-body">
							<div class="form-group">

								<input type="text" class="form-control" id="#" placeholder="Deskripsi . . . . " name="deskripsi">
							</div>

							<div class="form-group">
								<input type="file" id="exampleInputFile" name="userfile">
							</div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer">

							<button type="submit" class="btn btn-primary" value="upload"><i class="fa fa-cloud-upload"></i> Upload Gambar</button>
						</div>
					</form>

				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>No</th>
								<th>Nama Pt</th>
								<th>Gambar logo</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
            $no=1;
            if(!empty($logo)){
              foreach($logo as $fl){
                ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $fl->nama_logo_p; ?></td>
								<td>
                          <img src="<?php echo base_url(); ?>logo_perusahaan/<?php echo $fl->gambar_logo; ?>" width="100" height="100"></td>
                          <?php 
									if ($fl->status=='0'){
									echo '<td>Non Aktif</td>';
									}elseif($fl->status=='1'){
										echo '<td>Aktif</td>';
									}
								?>
								<td class="text-center">
						<?php 
									if($fl->status=='0'){
						echo "<a href="; echo site_url('File_controller/set_aktif/'.$fl->id_logo); echo " class='btn btn-success ' ><i class='fa fa-eye'></i></a>";
					}elseif($fl->status=='1'){
						echo "<a href="; echo site_url('File_controller/set_nonaktif/'.$fl->id_logo); echo " class='btn btn-success ' ><i class='fa fa-eye-slash'></i></a>";
							}
						?>
								<a href="<?php echo site_url('File_controller/hapus_logo/'.$fl->id_logo); ?>" class="btn btn-danger " onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
								</td>


							</tr>
						<?php } } ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>


<?php $this->load->view('admin/footer'); ?>

