<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url();?>asetad/dist/img/admin.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Admin ' '</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>


    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">NAVIGASI SISTEM</li>
      <li class="active">
        <a href="<?php echo base_url('admin/index'); ?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard Admin</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-tv"></i>
          <span>Master</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
         <li><a href="<?php echo base_url('admin/data_loker'); ?>"><i class="fa fa-circle-o"></i> Data Lowongan Kerja</a></li>
         <li><a href="<?php echo base_url('admin/show_alumni'); ?>"><i class="fa fa-circle-o"></i> Data Alumni</a></li>
         <li><a href="<?php echo base_url('admin/data_perusahaan'); ?>"><i class="fa fa-circle-o"></i> Data Perusahaan</a></li>
         <li><a href="<?php echo base_url('admin/p_kerja'); ?>"><i class="fa fa-circle-o"></i> Data Pencari Kerja</a></li>
         <li><a href="<?php echo base_url('admin/berkas');?>"><i class="fa fa-circle-o"></i> Data Berkas</a></li>
         <li><a href="<?php echo base_url('admin/user'); ?>"><i class="fa fa-circle-o"></i> Data User</a></li>
       </ul>
     </li>
     
     
     <li class="treeview">
      <a href="#">
        <i class="fa fa-pencil"></i> <span>Tulis Artikel</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
       <li><a href="<?php echo base_url('admin/beritaList');?>"><i class="fa fa-circle-o"></i> Berita</a></li>
       <li><a href="<?php echo base_url('admin/agendaList');?>"><i class="fa fa-circle-o"></i> Agenda</a></li>
       
     </ul>
   </li>
   <li class="treeview">
    <a href="#">
      <i class="fa fa-photo"></i> <span>Setting Logo Sponsorship</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo base_url('admin/galeri');?>""><i class="fa fa-circle-o"></i> Logo Perusahaan</a></li>
    </ul>
    <li>
      <a href="<?php echo base_url('admin/profil'); ?>">
        <i class="fa fa-cog"></i> <span>Setting</span>
      </a>
    </li>
    <li>
      <a href="<?php echo base_url();?>">
        <i class="fa fa-home"></i> <span>Home</span>
      </a>
    </li>
  </li>        
</ul>

</section>
<!-- /.sidebar -->
</aside>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $judul; ?>
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-home"></i>  <?php echo $judul; ?></a></li>
      <li class="active"> <?php echo $sub_judul; ?></li>
    </ol>
  </section>

  <div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert">
      <i class="ace-icon fa fa-times"></i>
    </button>
    <i class="ace-icon green"></i>
    <strong>Sistem Aplikasi Bursa Khusus Kerja || SMKN 1 CIJULANG<small> (v1.0) </small></strong>
  </div>