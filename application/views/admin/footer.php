 </div> <!-- content-wrapper -->
 <footer class="main-footer">
 	
 	<strong>Copyright &copy; 2019 <a href="#">BKK SMKN 1 CIJULANG</a>.</strong> All rights
 	reserved.
 </footer>

 <script src="<?php echo base_url();?>asetad/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url();?>asetad/tinymce/tinymce.min.js"></script>
 <script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        height: 300,
        theme: 'modern',
        plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insert file undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        file_picker_types: 'file image media',
        image_advtab: true
    });
</script>
 <!-- Bootstrap 3.3.7 -->
 <script src="<?php echo base_url();?>asetad/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
 <!-- DataTables -->
<script src="<?php echo base_url();?>asetad/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asetad/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <!-- FastClick -->
 <script src="<?php echo base_url();?>asetad/bower_components/fastclick/lib/fastclick.js"></script>
 <!-- AdminLTE App -->
 <script src="<?php echo base_url();?>asetad/dist/js/adminlte.min.js"></script>
 <!-- Sparkline -->
 <script src="<?php echo base_url();?>asetad/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
 <!-- jvectormap  -->
 <script src="<?php echo base_url();?>asetad/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
 <script src="<?php echo base_url();?>asetad/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
 <!-- SlimScroll -->
 <script src="<?php echo base_url();?>asetad/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
 <!-- ChartJS -->
 <script src="<?php echo base_url();?>asetad/bower_components/Chart.js/Chart.js"></script>
 <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
 <script src="<?php echo base_url();?>asetad/dist/js/pages/dashboard2.js"></script>
 <!-- AdminLTE for demo purposes -->
 <script src="<?php echo base_url();?>asetad/dist/js/demo.js"></script>
 <script>
 	$(document).ready(function () {
 		$('.sidebar-menu').tree()
 	})
 </script>
 <script>
  $(function () {
    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : false,
      'autoWidth'   : false


    })
  })
</script>
</body>
</html>

