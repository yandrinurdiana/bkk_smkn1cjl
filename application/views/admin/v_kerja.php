<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <link rel="icon" type="image/png" href="<?php echo base_url();?>asetad/dist/img/faviconsmk.png" />
  <title>Bursa Khusus Kerja | SMKN 1 Cijulang</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>asetad/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>asetad/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>asetad/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>asetad/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>asetad/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>asetad/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="<?php echo base_url();?>asetad/dist/css/skins/_all-skins.min.css">

   <!-- Google Font -->
   <link rel="stylesheet"
   href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 </head>

 <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>B</b>KK</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>BursaKhususKerja</b></span>
      </a>

      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
              <a class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>asetad/dist/img/admin.png" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata('ses_nama'); ?></span> 
              </a>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <li>
              <a href="<?php echo base_url('login_controller/logout'); ?>">Log out <span class="fa fa-sign-out"></span></a>
           </li>
         </ul>
       </div>
     </nav>
   </header>  <!-- akhir header-->

<?php $this->load->view('admin/sidebar'); ?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box box-primary">


					<!-- /.box-header -->
					<!-- form start -->

					<div class="box-footer">
						
					</div>

				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>no</th>
								<th class="text-center">Nama</th>
								<th>No Hp</th>
								<th>Tahun Lulus</th>
								<th>Asal Sekolah</th>
								<th>Usia</th>
								<th>Jurusan</th>
								<th>Detail</th>
							</tr>
						</thead>
						
						<tbody>
							<?php
                      $no=1;
                      foreach ($pelamar as $dk) {
                      ?>
							<tr>
								<td><?php echo $no++;?></td>
								<td><?php echo $dk->nama; ?></td>
								<td><?php echo $dk->no_hp; ?></td>
								<td><?php echo $dk->tahun_lulus; ?></td>
								<td><?php echo $dk->asal_sekolah; ?></td>
								<td><?php echo $dk->usia; ?>&nbsp;Tahun</td>
								<td><?php echo $dk->jurusan; ?></td>
								<td>
<a href="" class="btn btn-info" data-toggle="modal" data-target="#yourModal<?php echo $dk->id_umum; ?>"><i class='fa fa-align-justify'></i></a><?php if($dk->status=='0'){
						echo "<a href="; echo base_url().'umum_reg/umum_ok/'.$dk->id_umum; echo " class='btn btn-danger' ><i class='fa fa-check-circle'></i></a>";
					}else{
						
					}
					?>
								</td><div class="modal fade" id="yourModal<?php echo $dk->id_umum; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

									<h4 class="modal-title" id="myModalLabel"> Pencari Kerja</h4>
								</div>
								<div class="modal-body">
									<p><label>Nama : </label>&nbsp;
												<?php echo $dk->nama; ?></p>
												<hr>
								<p><label>No Hp :</label>&nbsp;<?php echo $dk->no_hp; ?></p>
												<hr>
								<p><label>Tanggal Lahir :</label>&nbsp;<?php echo $dk->tgl_lahir; ?></p>
												<hr>
								<p><label>Alamat Lengkap :</label>&nbsp;<?php echo $dk->alamat; ?></p>
												<hr>
								<p><label>Jenis Kelamin :</label>&nbsp;<?php echo $dk->jk; ?></p>
												<hr>
								<p><label>Email :</label>&nbsp;<?php echo $dk->email; ?></p>
												<hr>
								<p><label>Tahun Lulus :</label>&nbsp;<?php echo $dk->tahun_lulus; ?></p>
												<hr>
								<p><label>Asal Sekolah :</label>&nbsp;<?php echo $dk->asal_sekolah; ?></p>
												<hr>
								<p><label>Usia :</label>&nbsp;<?php echo $dk->usia; ?>&nbsp;Tahun</p>
												<hr>
								<p><label>Tinggi Badan :</label>&nbsp;<?php echo $dk->tinggi_badan; ?></p>
												<hr>
								<p><label>Jurusan :</label>&nbsp;<?php echo $dk->jurusan; ?></p>
												<hr>
								<p><label>Nilai UN Matematika :</label>&nbsp;<?php echo $dk->nilai_un_matematika; ?></p>
												<hr>
								<p><label>Nilai UN Rata-Rata :</label>&nbsp;<?php echo $dk->nilai_un_total; ?></p>
												<hr>
								<p><label>Tujuan Perusahaan :</label><br>Nama :&nbsp;<?php echo $dk->nama_perusahaan; ?><br>Posisi Kerja :&nbsp;<?php echo $dk->posisi_kerja; ?></p>
												<hr>
								<p><label>Status :</label>&nbsp;<?php if ($dk->status=='0') {
													echo "<span style='color:red'>Belum Terdaftar</span";
												}else{

												 echo "<span style='color:green'>Sudah Terdaftar</span";}?></p>
												 	<hr>
								</div>

							</div>
						</div>
					</div>
							</tr><?php  }?>
						</tbody>


					</table>

					 
					
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

</div> <!-- content-wrapper -->
<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 2.4.0
	</div>
	<strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
	reserved.
</footer>

<script src="<?php echo base_url();?>asetad/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>asetad/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asetad/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>asetad/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>asetad/dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>asetad/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url();?>asetad/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>asetad/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>asetad/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url();?>asetad/bower_components/Chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url();?>asetad/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>asetad/dist/js/demo.js"></script>
<script>
	$(document).ready(function () {
		$('.sidebar-menu').tree()
	})
</script>
<script>
	$(function () {
		$('#example1').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : true,
			'info'        : false,
			'autoWidth'   : false


		})
	})
	$(document).ready(function() {
		$('#example').DataTable( {
			dom: 'Bfrtip',
			buttons: [
			'copyHtml5',
			'excelHtml5',
			'pdfHtml5'
			]
		} );
	} );
</script>
</body>
</html>



