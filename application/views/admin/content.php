<section class="content">
      <!-- Info boxes -->
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
               
              <p>Jumlah Alumni</p>
              
              <h3><?php echo $alumni; ?></h3>
           
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <p>Perusahaan</p>
              <h3><?php echo $perusahaan; ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-bank"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <p>User</p>
              <h3><?php echo $siswa; ?></h3>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <p>Lowongan Kerja</p>
              <h3><?php echo $loker; ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
      </div>

      <div class="callout callout-danger">
          <h4><i class="fa fa-bullhorn"></i> Tip!</h4>

          <p>pemberitahuan.</p>
        </div>
    </section>