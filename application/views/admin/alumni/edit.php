<?php $this->load->view('./admin/header'); ?>

<?php $this->load->view('./admin/sidebar'); ?>


<section class="content" >
	<div class="row" >
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Form Pendafaran Alumni</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form role="form" action="<?php echo base_url().'admin/update_alumni'; ?>" method="post" >
					<?php
					
					foreach ($alumni as $am) {
					?>
					<input type="hidden" name="id_alumni" value="<?php echo $am->id_alumni; ?>">
					<div class="box-body">
						<a href="<?php echo base_url('alumni/lihat'); ?>" class="btn btn-info pull-right">Lihat Alumni</a>
						<p class="help-block">Silahkan isi formulir berikut dengan benar. 
						Isian email dan telp digunakan untuk pemberitahuan kegiatan alumni.</p>
						<div class="form-group">
							<label for="validationDefault01">Nomor NIS</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="No NIS" name="no_nis" value="<?php echo $am->no_nis; ?>" required>
						</div>
						<div class="form-group">
							<label for="validationDefault01">Nama Lengkap</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="Nama Lengkap..."  name="nama_lengkap" value="<?php echo $am->nama_lengkap; ?>" required>
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<textarea class="form-control" rows="3" placeholder="Enter ..." name="alamat"><?php echo $am->alamat; ?></textarea>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"  name="email" value="<?php echo $am->email; ?>" required>
						</div>
						<div class="form-group">
							<label for="validationDefault01">Nomor HP</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="08123xxxx" name="no_hp" value="<?php echo $am->no_hp; ?>" required>
						</div>
						
						<div class="form-group">
							<label>Pekerjaan</label>
							<select class="form-control" name="pekerjaan">
								<option value="Pilih">Pilih</option>
								<option <?php if($am->pekerjaan == "Pegawai Negeri"){ echo 'selected="selected"'; } ?> value="Pegawai Negeri">Pegawai Negeri</option>
								<option <?php if($am->pekerjaan == "Pegawai Swasta"){ echo 'selected="selected"'; } ?> value="Pegawai Swasta">Pegawai Swasta</option>
								<option <?php if($am->pekerjaan == "Guru"){ echo 'selected="selected"'; } ?> value="Guru">Guru</option>
								<option <?php if($am->pekerjaan == "Dosen"){ echo 'selected="selected"'; } ?> value="Dosen">Dosen</option>
								<option <?php if($am->pekerjaan == "Wiraswasta"){ echo 'selected="selected"'; } ?> value="Wiraswasta">Wiraswasta</option>
								<option <?php if($am->pekerjaan == "Kuliah"){ echo 'selected="selected"'; } ?> value="Kuliah">Kuliah</option>
								<option <?php if($am->pekerjaan == "Lain-lain"){ echo 'selected="selected"'; } ?> value="Lain-lain">Lain-lain</option>
							</select>
						</div>
						<div class="form-group">
							<label for="validationDefault01">Tempat Kerja/Kuliah</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="Tempat Kerja" name="tempat_kerja"  value="<?php echo $am->tempat_kerja; ?>" required>
						</div>
						<div class="form-group">
							<label>Tahun Lulus</label>
							
							<select id="year" class="form-control" name="tahun_lulus">
								<?php 
								$start = 2008;
								$end = date("Y"); ;
								for ($year=$start; $year <=$end; $year++) { ?>
									<option <?php if($am->tahun_lulus == $year){ echo 'selected="selected"'; } ?>value="<?php echo $year; ?>" ><?php echo $year; ?></option>
								<?php }
								?>
							</select>
							
						</div>
						<div class="form-group">
							<label for="validationDefault01">Jurusan</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="Jurusan" name="jurusan" value="<?php echo $am->jurusan; ?>" required>
						</div>
						<div class="form-group">
							<label>Pendidikan Terakhir</label>
							<select class="form-control" name="pendidikan_terakhir">
								<option>Pilih</option>
								<option <?php if($am->pendidikan_terakhir == "SMK"){ echo 'selected="selected"'; } ?> value="SMK">SMK</option>
								<option <?php if($am->pendidikan_terakhir == "D1"){ echo 'selected="selected"'; } ?> value="D1">D1</option>
								<option <?php if($am->pendidikan_terakhir == "D2"){ echo 'selected="selected"'; } ?> value="D2">D2</option>
								<option <?php if($am->pendidikan_terakhir == "D3"){ echo 'selected="selected"'; } ?>value="D3">D3</option>
								<option <?php if($am->pendidikan_terakhir == "S1"){ echo 'selected="selected"'; } ?>value="S1">S1</option>
								<option <?php if($am->pendidikan_terakhir == "S2"){ echo 'selected="selected"'; } ?>value="S2">S2</option>
								<option <?php if($am->pendidikan_terakhir == "S3"){ echo 'selected="selected"'; } ?>value="S3">S3</option>
							</select>
						</div>
						<div class="form-group ">
							<label>Komentar/Pesan/Kesan</label>
							<textarea class="form-control" rows="3" placeholder="Enter ..." name="pesan"><?php echo $am->pesan; ?></textarea>
						</div>
						<button type="reset" class="btn btn-default">Ulangi</button>
						<button type="submit" class="btn btn-primary pull-right">Kirim</button>
					</div>
					<!-- /.box-body -->
				<?php } ?>
				</form>

			</div>
			<!-- /.box -->
		</div>
		<!--/.col (right) -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->

<?php $this->load->view('./admin/footer'); ?>

