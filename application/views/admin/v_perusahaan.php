<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('admin/sidebar'); ?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box box-primary">


					<!-- /.box-header -->
					<!-- form start -->

					

				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>no</th>
								<th class="text-center">Nama Perusahaan</th>
								<th>Alamat Perusahaan</th>
								<th>Nomor Telpon Perusahaan</th>
								<th>Email Perusahaan</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no=1;
							foreach ($perusahaan as $prh) {
								?>
								<tr>
									<td><?php echo $no++;?></td>
									<td><?php echo $prh->nama_perusahaan;?></td>
									<td><?php echo $prh->alamat_perusahaan;?></td>
									<td><?php echo $prh->telp_perusahaan;?></td>
									<td><?php echo $prh->email;?></td>
									<td class="text-center">
                        <a href="<?php echo site_url('page_perushaan_controller/edit_perusahaan_admin/'.$prh->id_log_ph); ?>" class="btn btn-success " ><i class="fa fa-pencil"></i></a>
                       <!--  <a href="javascript:;" onclick="return isconfirm('<?php echo site_url("page_perushaan_controller/hapus_perusahaan_admin/".$prh->id_log_ph); ?>');" class="btn btn-danger " ><i class="fa fa-trash"></i></a> -->

                      </td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>

	<script type="text/javascript">
    function isconfirm(url_val){
    
    if(confirm('Anda yakin hapus akun perushaan ini?') == false)
    {
        return false;
    }
    else
    {
        location.href=url_val;
    }
}
</script>


	<?php $this->load->view('admin/footer'); ?>