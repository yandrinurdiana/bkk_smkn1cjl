

<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('admin/sidebar'); ?>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-primary">


          <!-- /.box-header -->
          <!-- form start -->

          <div class="box-footer">
            
          </div>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">

             <thead>
                      <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Jurusan</th>
                        <th>Tahun Lulus</th>
                        <th>Pendidikan Terakhir</th>
                        <th>No. Telp</th>
                        <th>Pekerjaan</th>
                        <th>Tempat Kerja</th> 
                        <th>Alamat</th>
                        <th>Pesan Kesan</th>
                        <th>action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $no=1;
                      foreach ($alumni as $am) {
                      ?>
                      <tr>
                        <td><?php echo $no++;?></td>
                       <td><?php echo $am->no_nis; ?></td>
                       <td><?php echo $am->nama_lengkap; ?></td>
                       <td><?php echo $am->jurusan; ?></td>
                       <td><?php echo $am->tahun_lulus; ?></td>
                       <td><?php echo $am->pendidikan_terakhir; ?></td>
                       <td><?php echo $am->no_hp; ?></td>
                       <td><?php echo $am->pekerjaan; ?></td>
                       <td><?php echo $am->tempat_kerja; ?></td>
                       <td><?php echo $am->alamat; ?></td>
                       <td><?php echo $am->pesan; ?></td>
                        <td class="text-center">
                          <a href="<?php echo site_url('admin/edit_alumni/'.$am->id_alumni); ?>" class="btn btn-success " ><i class="fa fa-pencil"></i></a>
                          <a href="<?php echo site_url('admin/hapus_alumni/'.$am->id_alumni); ?>" class="btn btn-danger " onclick="return confirm('Are you sure?')" ><i class="fa fa-trash"></i></a>
                        </td>
                        
                        
                      </tr>
                    <?php } ?>
                    </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>


<?php $this->load->view('admin/footer'); ?>