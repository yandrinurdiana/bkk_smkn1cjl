<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('admin/sidebar'); ?>


<section class="content" >
	<div class="row" >
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Form Pendafaran Perusahaan</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				 <?php foreach ($perusahaan as $prh) {?>
				<form role="form" action="<?php echo base_url('page_perushaan_controller/update_perusahaan_admin'); ?>" method="post" >
					<input type="hidden" name="id_log_ph" value="<?php echo $prh->id_log_ph; ?>">
					<div class="box-body">
						<a href="<?php echo base_url('admin/data_perushaan'); ?>" class="btn btn-info pull-right">Lihat data Perusahaan</a>
						<p class="help-block">Silahkan isi formulir berikut dengan benar. 
						Isian email dan telp digunakan untuk pemberitahuan kegiatan alumni.</p>
						<div class="form-group">
							<label for="validationDefault01"> Nama Perusahaan</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="Nama Perusahaan" name="nama_perusahaan" value="<?php echo $prh->nama_perusahaan; ?>" required>
						</div>
						<div class="form-group">
							<label for="validationDefault01">No Telepon</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="No Hp Perusahaan"  name="telp_perusahaan" value="<?php echo $prh->telp_perusahaan; ?>" required>
						</div>
						<div class="form-group">
							<label>Alamat Perusahaan</label>
							<textarea class="form-control" rows="3" placeholder="Alamat Perusahaan" name="alamat_perusahaan"><?php echo $prh->alamat_perusahaan; ?>"
							</textarea>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email Perusahaan</label>
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"  name="email" value="<?php echo $prh->email; ?>" required>
						</div>				
						
						<button type="reset" class="btn btn-default">Ulangi</button>
						<button type="submit" class="btn btn-primary pull-right">Kirim</button>
					</div>
					<!-- /.box-body -->
				</form>
				 <?php } ?>
			</div>
			<!-- /.box -->
		</div>
		<!--/.col (right) -->
	</div>
	<!-- /.row -->
</section>
<?php $this->load->view('admin/footer'); ?>