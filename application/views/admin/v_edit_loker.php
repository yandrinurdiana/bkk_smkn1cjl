<!-- <awalan yang baik untuk perusahaan> -->

<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('admin/sidebar'); ?>


      <!-- awal konten tengah -->
      <section class="content">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Info Lowongan Pekerjaan</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
            <?php foreach ($loker as $u) {
    ?>
              <?php echo form_open_multipart('page_perushaan_controller/update_upload_admin'); ?>
                <div class="box-body">
                  <div class="form-group">
                    <label for="text">Nama Perusahaan</label>
                    <input type="hidden" name="id_lk" value="<?php echo $u->id_lk; ?>">
                    <input type="hidden" name="old_image" value="<?php echo $u->image; ?>">
                    <input type="text" class="form-control" id="text" placeholder="Nama Perusahaan" name="nama_perusahaan" value="<?php echo $u->nama_perusahaan; ?>">
                  </div>
                  <div class="form-group">
                    <label for="text">Posisi</label>
                    <input type="text" class="form-control" id="text" placeholder="Posisi Bekerja" name="posisi_kerja" value="<?php echo $u->posisi_kerja; ?>">
                  </div>
                  <div class="form-group">
                    <label for="text">Kontak</label>
                    <input type="text" class="form-control" id="text" placeholder="Kontak" name="kontak_person"  value="<?php echo $u->kontak_person; ?>">
                  </div>
                  <div class="form-group">
                    <label for="text">Tanggal Berakhir</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="date" class="form-control pull-right" id="datepicker" name="tanggal_kadaluarsa" value="<?php echo $u->tanggal_kadaluarsa; ?>">
                    </div>
                    <div class="form-group">
                      <label>Deskripsi</label>
                      <textarea class="form-control" rows="3" placeholder="Deskripsi" name="deskripsi"><?php echo $u->deskripsi; ?> </textarea>
                    </div>
                    <div class="form-group">
                    <label for="text">Kuota Loker</label>
                    <input type="text" class="form-control" id="text" placeholder="Kuota loker" name="kuota_loker"  value="<?php echo $u->kuota; ?>">
                  </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile" name="image" ><?php echo $u->image; ?>
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="reset" class="btn btn-flat">Reset</button>
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                  </div>
                  <?php echo form_close(); ?>
               
                <?php
} ?>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer -->
          </section>

        </div>

<!-- Add the sidebar's background. This div must be placed
 immediately after the control sidebar -->
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('admin/footer'); ?>