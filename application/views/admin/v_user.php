<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('admin/sidebar'); ?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box box-primary">


					<!-- /.box-header -->
					<!-- form start -->

					<div class="box-footer">
						<a href="<?php echo base_url('admin/berita');?>"><button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Berita</button></a>
					</div>

				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>No</th>
								<th class="text-center">User Name</th>
								<th>Email</th>
								<th>Level</th>
								<th>Active</th>
								<th>terakhir login</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>ucang</td>
								<td>Hnugra@gmailc.om</td>
								<td>admin</td>
								<td>1</td>
								<td>2019-08-17 02:06:12</td>
								
								<td class="text-center">
									<a href="<?php echo site_url('page_perushaan_controller/edit_loker/'); ?>" class="btn btn-success " ><i class="fa fa-pencil"></i></a>
									<a href="<?php echo site_url('page_perushaan_controller/hapus_loker/'); ?>" class="btn btn-danger " ><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>


<?php $this->load->view('admin/footer'); ?>