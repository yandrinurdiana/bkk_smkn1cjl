<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <link rel="icon" type="image/png" href="<?php echo base_url();?>asetad/dist/img/faviconsmk.png" />
    <meta name="author" content="">
    <title>Bursa Kerja Khusus || SMKN 1 Cijulang </title>
    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()."asethal" ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url()."asethal" ?>/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>asetad/bower_components/font-awesome/css/font-awesome.min.css">
    
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>asetad/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>asetad/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- Plugin CSS -->
    <link href="<?php echo base_url()."asethal" ?>/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()."asethal" ?>/css/creative.min.css" rel="stylesheet">
</head>