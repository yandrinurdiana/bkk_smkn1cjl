

<!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url()."asethal" ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url()."asethal" ?>/vendor/popper/popper.min.js"></script>
    <script src="<?php echo base_url()."asethal" ?>/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url()."asethal" ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url()."asethal" ?>/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="<?php echo base_url()."asethal" ?>/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url()."asethal" ?>/js/creative.min.js"></script>

    
</body>
</html>