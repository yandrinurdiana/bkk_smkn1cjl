

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" >
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Bursa Khusus Kerja SMKN 1 Cijulang </a> 
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">Tentang</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Services</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#portfolio">Perusahaan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#lowongan">Lowongan</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Kontak</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."tb_alumni" ?>">Berkas</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."welcome/alumni" ?>">Alumni</a>
          </li>


          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."welcome/login" ?>">Login</a>
          </li>
        </ul>

      </div>
    </div>
  </nav>
  <header class="masthead">
    <div class="header-content">
      <div class="header-content-inner">
        <h1 id="homeHeading">Bursa Khusus Kerja <br>SMKN 1 cijulang</h1>
        <hr>
        <p>Selamat Datang di Situs Resmi BKK SMKN 1 Cijulang, Temukan Pekerjaan Idaman Anda</p>
        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Temukan Perkerjaanmu</a>
      </div>
    </div>
  </header>
  <!-- <awal untuk alumni mencari data diri> -->
   <div id="lowongan" class="call-to-action bg-dark">
    <di v class="container"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-lg-12 text-center"> 
            <h2>PENELUSURAN ALUMNI SMKN 1 CIJULANG</h2> 
            <div class="input-group">
              
            </div>
            <h2 align="center">Cari NIS/Nama</h2><br />
            <div class="form-group">
              <div class="input-group">
               <span class="input-group-addon">Search</span>
               <input type="text" name="search_text" id="search_text" placeholder="Masukan NIS/Nama anda" class="form-control" />
             </div>
           </div>
           <br />
           <div id="result"></div>
         </div>
         <div style="clear:both"></div>
         <br />
         <br />
         <br />
         <br />
         <!-- /input-group -->
         
         
          <!-- <form  id="search_alumni" >
          <div class="input-group input-group-sm">
            <input type="text" class="form-control" name="cari_data">
            <span class="input-group-btn">
             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detail" class="btn btn-info">Cari <i class="fa fa-search"></i></button>
           
           </span>
         </form> -->
       </div>
     </div>
   </div>
   <!-- /.row -->
 </div>
 <!-- /.container -->
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="detail" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title">Detail Alumni</h4>
      </div>
      <div class="modal-body">
        <div class="detail-alumni"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){

   load_data();

   function load_data(query)
   {
    $.ajax({
     url:"<?php echo base_url(); ?>welcome/fetch",
     method:"POST",
     data:{query:query},
     success:function(data){
      $('#result').html(data);
    }
  })
  }

  $('#search_text').keyup(function(){
    var search = $(this).val();
    if(search != '')
    {
     load_data(search);
   }
   else
   {
     load_data();
   }
 });
});
</script>

<script type="text/javascript">
  var base_url = "<?php echo base_url();?>";


  $("#search_alumni").ready(function(){
    $('#detail').on('show.bs.modal', function (e) {
      var cari_data = $(e.relatedTarget).data('id');
      $.ajax({
        type : 'post',
        url: base_url + "welcome/search_alumni", 
        data :  $('#search_alumni').serialize(),
        success : function(data){
          console.log('sukses');

        $('.detail-alumni').html(data);//menampilkan data ke dalam modal
      }
    });
    });
  });  
</script>