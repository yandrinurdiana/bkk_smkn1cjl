<?php $this->load->view('alumni/v_header'); ?>

<?php $this->load->view('alumni/v_sidebar'); ?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Lowongan Pekerjaan</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example1" class="table table-bordered table-hover">

						<thead>
							<tr>
								<th>no</th>
								<th class="text-center">Nama Perusahaan</th>
								<th>Posisi</th>
								<th>Kontak</th>
								<th>Tanggal Berakhir</th>
								<th>Deskripsi</th>
								<th>Detail</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>4</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>6</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>7</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>8</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>9</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>10</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/23/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>11</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah aki </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>12</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>0838</td>
								<td>1/1/2020</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

<?php $this->load->view('alumni/v_footer'); ?>

