<?php $this->load->view('alumni/v_header'); ?>

<?php $this->load->view('alumni/v_sidebar'); ?>

<!-- awal konten tengah -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Alumni</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <a href="<?php echo base_url('alumni/daftar'); ?>" class="btn btn-info ">Daftar Alumni</a>
                    <thead>
                      <tr class="text-center">
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Jurusan</th>
                        <th>Tahun Lulus</th>
                        <th>Pekerjaan</th>
                        <th>Pendidikan Terakhir</th>
                        <th>No. Telp</th>
                        <th>Pekerjaan</th>
                        <th>Tempat Kerja</th> 
                        <th>Alamat</th>
                        <th>Pesan Kesan</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                       <?php 
      $no = 1;
      foreach ($alumni as $am) {
          ?>
                      <tr>
                        <td><?php echo $am->no_nis; ?></td>
                        <td><?php echo $am->nama_lengkap; ?></td>
                        <td><?php echo $am->jurusan; ?></td>
                        <td><?php echo $am->tahun_lulus; ?></td>
                        <td><?php echo $am->pendidikan_terakhir; ?></td>
                        <td><?php echo $am->email; ?></td>
                        <td><?php echo $am->no_hp; ?></td>
                        <td><?php echo $am->pekerjaan; ?></td>
                        <td><?php echo $am->tempat_kerja; ?></td>
                        <td><?php echo $am->alamat; ?></td>
                        <td><?php echo $am->pesan; ?></td>
                        
                      </tr>
                        <?php
      } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
        <!-- akhir seksion-->

<?php $this->load->view('alumni/v_footer'); ?>

