 <?php $this->load->view('alumni/v_header'); ?>

 <?php $this->load->view('alumni/v_sidebar'); ?>

 <!-- Main content -->
 <section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>asetad/dist/img/avatar5.png" alt="User profile picture">

          <h3 class="profile-username text-center"><?php echo $this->session->userdata('ses_nama'); ?></h3>

          <p class="text-muted text-center">Software Engineer</p>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->

    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#activity" data-toggle="tab">Basic Information</a></li>
          <li><a href="#settings" data-toggle="tab">Change Password</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="activity">
            <!-- Post -->
            <div class="post">

              <!-- /.user-block -->
              <form class="form-horizontal">
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputName">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="password"  class="form-control" id="inputEmail" >
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Avatar</label>
                  <div class="col-sm-10">
                    <input type="file"  class="form-control" id="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-danger">Simpan</button>
                  </div>
                </div>
              </form>
              <!-- /.row -->
            </div>
            <!-- /.post -->
          </div>
          <!-- /.tab-pane -->

          <div class="tab-pane" id="settings">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Password Lama</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" id="myInput" placeholder="Password Lama">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Password Baru</label>
                <div class="col-sm-10">
                  <input type="password"  class="form-control" id="myInput" placeholder="Password Baru">
                </div>
              </div>

              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Konfirmasi</label>
                <div class="col-sm-10">
                  <input type="password"  class="form-control" id="myInput" placeholder="Konfirmasi">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" onclick="myFunction()"> I agree to the <a href="#">terms and conditions</a>
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-danger">Submit</button>
                </div>
              </div>
            </form>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

</section>
<!-- /.content -->
<?php $this->load->view('alumni/v_footer'); ?>