
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>asetad/dist/img/avatar5.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alumni ''</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">NAVIGASI SISTEM</li>
      <li class="active">
        <a href="<?php echo base_url('alumni'); ?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard Alumni</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-tv"></i>
          <span>Master</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('alumni/loker'); ?>"><i class="fa fa-circle-o"></i>Lowongan Kerja</a></li>
          <li><a href="<?php echo base_url('alumni/lihat'); ?>"><i class="fa fa-circle-o"></i> Lihat Alumni</a></li>
        </ul>
      </li>    
      <li>
        <a href="<?php echo base_url('admin/profil'); ?>">
          <i class="fa fa-cog"></i> <span>Setting</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url(); ?>">
          <i class="fa fa-home"></i> <span>Home</span>
        </a>
      </li>
    </li>        
  </ul>
</section>
<!-- /.sidebar -->
</aside>

<!-- Content Wrapper Atas-->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $judul; ?>
      <small>Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-home"></i>  <?php echo $judul; ?></a></li>
      <li><a href="#"><?php echo $sub_judul; ?></a></li>
      <li class="active"> <?php echo $sub_judul; ?></li>
    </ol>
  </section>