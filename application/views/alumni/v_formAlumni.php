<?php $this->load->view('alumni/v_header'); ?>

<?php $this->load->view('alumni/v_sidebar'); ?>


<section class="content" >
	<div class="row" >
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Form Pendafaran Alumni</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form role="form" action="<?php echo base_url().'alumni/insert_alumni'; ?>" method="post" >
					<div class="box-body">
						<a href="<?php echo base_url('alumni/lihat'); ?>" class="btn btn-info pull-right">Lihat Alumni</a>
						<p class="help-block">Silahkan isi formulir berikut dengan benar. 
						Isian email dan telp digunakan untuk pemberitahuan kegiatan alumni.</p>
						<div class="form-group">
							<label for="validationDefault01"> NIS</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="NIS" name="no_nis" required>
						</div>
						<div class="form-group">
							<label for="validationDefault01">Nama Lengkap</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="Nama Lengkap..."  name="nama_lengkap" required>
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<textarea class="form-control" rows="3" placeholder="Enter ..." name="alamat"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"  name="email" required>
						</div>
						<div class="form-group">
							<label for="validationDefault01">Nomor HP</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="08123xxxx" name="no_hp" required>
						</div>
						
						<div class="form-group">
							<label>Pekerjaan</label>
							<select class="form-control" name="pekerjaan">
								<option value="Pilih">Pilih</option>
								<option value="Pegawai Negeri">Pegawai Negeri</option>
								<option value="Pegawai Swasta">Pegawai Swasta</option>
								<option value="Guru">Guru</option>
								<option value="Dosen">Dosen</option>
								<option value="Wiraswasta">Wiraswasta</option>
								<option value="Kuliah">Kuliah</option>
								<option value="Lain-lain">Lain-lain</option>
							</select>
						</div>
						<div class="form-group">
							<label for="validationDefault01">Tempat Kerja/Kuliah</label>
							<input type="text" class="form-control" id="validationDefault01" placeholder="Tempat Kerja" name="tempat_kerja"  required>
						</div>
						<div class="form-group">
							<label>Tahun Lulus</label>
							<select id="year" class="form-control" name="tahun_lulus">
							</select>
						</div>
						<div class="form-group">
							<label for="validationDefault01">Jurusan</label>
							<select class="form-control" name="jurusan">
								<option value="Pilih">Pilih</option>
								<option value="TKJ">Teknik Komputer dan Jaringan</option>
								<option value="TKR">Teknik Kendaraan Ringan Otomotif</option>
								<option value="TSM">Teknik Bisnis Sepeda Motor</option>
								<option value="MM">Teknik Multimedia</option>
								<option value="AP">Akuntansi dan Keuangan Lembaga</option>
							</select>
						</div>
						<div class="form-group">
							<label>Pendidikan Terakhir</label>
							<select class="form-control" name="pendidikan_terakhir">
								<option>Pilih</option>
								<option value="SMK">SMK</option>
								<option value="D1">D1</option>
								<option value="D2">D2</option>
								<option value="D3">D3</option>
								<option value="S1">S1</option>
								<option value="S2">S2</option>
								<option value="S3">S3</option>
							</select>
						</div>
						<div class="form-group ">
							<label>Komentar/Pesan/Kesan</label>
							<textarea class="form-control" rows="3" placeholder="Enter ..." name="pesan"></textarea>
						</div>
						<button type="reset" class="btn btn-default">Ulangi</button>
						<button type="submit" class="btn btn-primary pull-right">Kirim</button>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
		<!--/.col (right) -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->
<?php $this->load->view('alumni/v_footer'); ?>

