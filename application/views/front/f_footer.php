<section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Hubungi Kami!</h2>
            <hr class="primary">
            <p>Jika ada pertanyaan ataupun keperluan bisa hubungi kami melalui alamat dibawah ini  </p>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fa fa-map-marker fa-3x sr-contact"></i>
            <p>Jl. Mayoor Raswiyan Desa Kondangjajar Kecamatan Cijulang Kabupaten Pangandaran kodepos 46394</p>
          </div>

          <div class="col-lg-4 ml-auto text-center">
            <i class="fa fa-phone fa-3x sr-contact"></i>
            <p>(0265)2640354 </p>
          </div>

          <div class="col-lg-4 mr-auto text-center">
            <i class="fa fa-envelope-o fa-3x sr-contact"></i>
            <p><a href="mailto:smkncijulang@gmail.com">smkncijulang@gmail.com </a></p>
          </div>
        
        </div>
      </div>
    </section>