<?php $this->load->view('front/f_head') ?>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="<?php echo base_url()."" ?>">Bursa Kerja Khusus SMKN 1 Cijulang </a> 
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."" ?>">Home</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Services</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Perusahaan</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Kontak</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."tb_alumni" ?>">Tabel</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."welcome/login" ?>">Login</a>
            </li>
          </ul>

        </div>
      </div>
    </nav>
    <header class="masthead">
      <div class="header-content">
        <div class="header-content-inner">
          <h1 id="homeHeading">Burasa Khusus Kerja <br>SMKN 1 cijulang</h1>
          <hr>
          <p>Selamat Datang di Situs Resmi BKK SMKN 1 Cijulang, Temukan Pekerjaan Yang Sesuai Dengan Persyaratan</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Temukan Perkerjaanmu</a>
        </div>
      </div>
    </header>
    <div id="lowongan" class="call-to-action bg-dark">
      <h2 class="text-center">Data Alumni SMKN 1 Cijulang</h2> 
      <div class="container">
        <div class="box">
          <div class="box-header"> </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('tb_alumni/create'),'Daftar', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('tb_alumni/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('tb_alumni'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>

              <table id="alumni" class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Tahun Lulus</th>
                <th>Pekerjaan</th>
            </tr><?php
            foreach ($tb_alumni_data as $tb_alumni)
            {
                ?>
                <tr>
                  <td width="80px"><?php echo ++$start ?></td>
                  <td><?php echo $tb_alumni->nama ?></td>
                  <td><?php echo $tb_alumni->jenis_kelamin ?></td>
                  <td><?php echo $tb_alumni->alamat ?></td>
                  <td><?php echo $tb_alumni->tahun_lulus ?></td>
                  <td><?php echo $tb_alumni->pekerjaan ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
            </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    <!-- <footer ada disini nich> -->
    
    <div class="call-to-action">
      <div class="container">
            <p>Copyright &copy; 2018 All rights reserved | Design by <a href="http://w3layouts.com">Bootstrap</a> | Edited by <a href="#">Nugraha</a></p>
        </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <?php $this->load->view('front/f_script') ?>

  </body>
</html>
