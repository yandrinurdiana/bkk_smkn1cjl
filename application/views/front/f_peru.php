<style>
.grid-container {
  display: grid;
  grid-template-columns: 46px 46px auto;
  background-color: #2196F3;
  padding: 10px;
}
.grid-item {
  background-color: rgba(255, 255, 255, 0.8);
  border: 1px solid rgba(0, 0, 0, 0.8);
  padding: 20px;
  font-size: 30px;
  text-align: center;

}
</style>
<div class="card pt-5 mb-0" style="background-image: url('assets/images/bg.jpg');">

  <div class="card-body text-success">
    <div class="container">
      <h1 class="text-center">perusahaan</h1>
      <p>perusahaan yang bermitra dengan kami</p>
      <div class="box-body">
          <div class="grid-container">
             <?php foreach ($logo as $fl) {
              
      if ($fl->status=='1') { ?> 
  <div class="grid-item"><img src="<?php echo base_url(); ?>logo_perusahaan/<?php echo $fl->gambar_logo; ?>" class="img-responsive" style="max-width: 20% !important; display: initial !important;" ></div>
 
<?php }} ?>
</div>
              <!-- /.box-body -->
            </div>
    </div>
  </div>
  
</div>