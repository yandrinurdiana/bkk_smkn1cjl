<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('admin/sidebar'); ?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box box-primary">

					<!-- /.box-header -->
					<!-- form start -->
					<form role="form">
						<div class="box-footer">

							<a href="<?php echo base_url('admin/agenda'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Berita</a><br>
						</div>
					</form>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example1" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th class="text-center">Judul</th>
								<th>Author</th>
								<th>Date</th>
								<th>Status</th>
								<th>Deskripsi</th>
								<th>Detail</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>

							<tr>
								<td>3</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>

							<tr>
								<td>4</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>

							<tr>
								<td>5</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>

							<tr>
								<td>6</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>

							<tr>
								<td>7</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>

							<tr>
								<td>8</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>9</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							<tr>
								<td>10</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>

							<tr>
								<td>11</td>
								<td>PT Maju Ga </td>
								<td>Admin</td>
								<td>1/1/2019</td>
								<td>Publish</td>
								<td>PT. Maju Terus Pantang Mundur adalah sebuah perusahaan </td>
								<td class="text-center">
									<a href="" class="btn btn-success " ><i class="fa fa-eye"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

<?php $this->load->view('admin/footer'); ?>

