
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-primary">
          <?php echo $error;?>
          <?php echo $message;?><br>
          <?php echo form_open_multipart('File_controller/do_upload');?>
          <div class="box-body">
            <div class="form-group">

              <input type="text" class="form-control" id="#" placeholder="Deskripsi . . . . " name="deskripsi">
            </div>

            <div class="form-group">
              <input type="file" id="exampleInputFile" name="userfile">
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            
            <button type="submit" class="btn btn-primary" value="upload"><i class="fa fa-cloud-upload"></i> Upload File</button>
          </div>
        </form>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Deskripsi</th>
              <th>Nama File</th>
              
              <th>Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $no=1;
            if(!empty($file)){
              foreach($file as $fl){
                ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $fl['deskripsi'] ?> </td>
                  <td><?php echo $fl['file_name'] ?> </td>
                  
                  <td><?php echo $fl['created'] ?> </td>
                  <td class="text-center">
                    
                    <a href="<?php echo base_url().'File_controller/download/'.$fl['id_file']; ?>" class="btn btn-succes"><i class="fa fa-download"></i></a>
                    

                    <a href="<?php echo site_url('File_controller/hapus_berkas/'.$fl['id_file']); ?>" class="btn btn-danger " >
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <?php }} ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>