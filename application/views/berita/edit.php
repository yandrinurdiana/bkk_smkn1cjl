<?php $this->load->view('berita/header'); ?>

<?php $this->load->view('berita/sidebar'); ?>
     <section class="content">
      <div class="row">
        
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Berita</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<?php foreach ($berita as $brt) {
    ?>
                <?php echo form_open_multipart('Berita_controller/update_berita'); ?>
               	<input type="hidden" name="id_berita" value="<?php echo $brt->id_berita; ?>">
                <input type="hidden" name="old_image" value="<?php echo $brt->image; ?>">
              <div class="form-group">
                <label for="exampleFormControlSelect1">Judul</label>
                <input class="form-control" type="text" name="judul" placeholder="Judul Berita" value="<?php echo $brt->judul; ?>" required>
              </div>
              
              <div class="form-group">
                <textarea class="textarea" placeholder="Place some text here"
            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px; " name="isi"><?php echo $brt->isi; ?></textarea>
              </div>
            </div>
            <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile" name="image" ><?php echo $brt->image; ?>
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
            <div class="form-group ">
                <label for="exampleFormControlSelect1">Status</label>
                <select class="form-control" id="exampleFormControlSelect1" name="status" required>
                  <option>Select</option>
                  <option <?php if($brt->status == "1"){ echo 'selected="selected"'; } ?> value="1">Published</option>
                  <option <?php if($brt->status == "0"){ echo 'selected="selected"'; } ?> value="0">Pending</option>
                </select>
              </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
               <a href="<?php echo base_url('admin/beritaList');?>"><button type="button" class="btn btn-default">Back</button></a>
               <button type="submit" class="btn btn-primary">Post</button>
             </div>
             <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Restart</button>
           </div>
          <?php echo form_close(); ?>
     <?php } ?>
           <!-- /.box-footer -->
         </div>
         <!-- /. box -->
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
   </section>
   <!-- /.content -->
<?php $this->load->view('berita/footer'); ?>
