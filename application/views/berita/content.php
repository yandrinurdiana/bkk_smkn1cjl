
<section class="content">
  <div class="row">

    <!-- /.col -->
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tulis Berita Baru</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
         
          <?php echo form_open_multipart('berita_controller/insert_berita'); ?>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Judul</label>
            <input class="form-control" type="text" name="judul" placeholder="Judul Berita" required>
          </div>

          <div class="form-group">
            <textarea class="textarea"
            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px; " name="isi"></textarea>
          </div>
        </div>
        <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile" name="image" >
                      <p class="help-block">Example block-level help text here.</p>
                    </div>

        <div class="form-group ">
          <label for="exampleFormControlSelect1">Status</label>
          <select class="form-control" id="exampleFormControlSelect1" name="status" required>
            <option>Select</option>
            <option value="1">Published</option>
            <option value="0">Pending</option>
          </select>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="pull-right">
           <a href="<?php echo base_url('admin/beritaList');?>"><button type="button" class="btn btn-default">Back</button></a>
           <button type="submit" class="btn btn-primary">Post</button>
         </div>
         <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Restart</button>
       </div>
     <?php echo form_close(); ?>
     <!-- /.box-footer -->
   </div>
   <!-- /. box -->
 </div>
 <!-- /.col -->

</div>
<!-- /.row -->
</section>
<!-- /.content -->

