 </div> <!-- content-wrapper -->
 <footer class="main-footer">
 	<div class="pull-right hidden-xs">
 		<b>Version</b> 2.4.0
 	</div>
 	<strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
 	reserved.
 </footer>



 <!-- jQuery 3 -->
 <script src="<?php echo base_url();?>asetad/bower_components/jquery/dist/jquery.min.js"></script>
 <script src="<?php echo base_url();?>asetad/tinymce/tinymce.min.js"></script>
 <script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        height: 300,
        theme: 'modern',
        plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insert file undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        file_picker_types: 'file image media',
        image_advtab: true
    });
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>asetad/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>asetad/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>asetad/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>asetad/dist/js/demo.js"></script>
<!-- CK Editor -->
<script src="<?php echo base_url();?>asetad/bower_components/ckeditor/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>asetad/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
})
</script>

</body>
</html>
