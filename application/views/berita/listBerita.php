<?php $this->load->view('admin/header'); ?>

<?php $this->load->view('berita/sidebar'); ?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box box-primary">

           
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-footer">
                <a href="<?php echo base_url('admin/berita');?>"><button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Berita</button></a>
              </div>
            
          </div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example1" class="table table-responsive table-hover">
						
						<thead>
							<tr>
								<th>No</th>
								<th class="text-center">Judul</th>
								<th>Author</th>
								<th>Date</th>
								<th>Isi</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($berita as $brt) {?>
								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $brt->judul; ?></td>
									<td><?php echo $brt->author; ?></td>
									<td><?php echo $brt->created_at; ?></td>
									<td><?php echo substr($brt->isi, 0, 200);  ?></td>
									<?php 
									if ($brt->status=='0'){
									echo '<td>Pending</td>';
									}elseif($brt->status=='1'){
										echo '<td>Published</td>';
									}
								?>
								<td class="text-center">
						<?php 
									if($brt->status=='0'){
						echo "<a href="; echo site_url('berita_controller/publish_berita/'.$brt->id_berita); echo " class='btn btn-success ' ><i class='fa fa-eye-slash'></i></a>";
					}elseif($brt->status=='1'){
						echo "<a href="; echo site_url('berita_controller/pending_berita/'.$brt->id_berita); echo " class='btn btn-success ' ><i class='fa fa-eye'></i></a>";
							}
						?>
                        <a href="<?php echo site_url('berita_controller/edit_berita/'.$brt->id_berita); ?>" class="btn btn-success " ><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo site_url('berita_controller/hapus_berita/'.$brt->id_berita); ?>" class="btn btn-danger " ><i class="fa fa-trash"></i></a>
                      </td>
								</tr>
							<?php } ?>
							</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

<?php $this->load->view('admin/footer'); ?>

