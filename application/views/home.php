<?php $this->load->view('front/f_head') ?>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" >
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Bursa Khusus Kerja SMKN 1 Cijulang </a> 
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Tentang</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Services</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Perusahaan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#lowongan">Lowongan</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Kontak</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."tb_alumni" ?>">Berkas</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."welcome/alumni" ?>">Alumni</a>
            </li>


            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url()."welcome/login" ?>">Login</a>
            </li>
          </ul>

        </div>
      </div>
    </nav>
    <header class="masthead">
      <div class="header-content">
        <div class="header-content-inner">
          <h1 id="homeHeading">Bursa Khusus Kerja <br>SMKN 1 cijulang</h1>
          <hr>
          <p>Selamat Datang di Situs Resmi BKK SMKN 1 Cijulang, Temukan Pekerjaan Idaman Anda</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Temukan Perkerjaanmu</a>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">Tentang Bursa Khusus Kerja</h2>
            <hr class="light">
            <p class="text-faded">Bursa Kerja Khusus (BKK) adalah sebuah lembaga yang dibentuk di Sekolah Menengah Kejuruan Negeri dan Swasta, sebagai unit pelaksana yang memberikan pelayanan dan informasi lowongan kerja, pelaksana pemasaran, penyaluran dan penempatan tenaga kerja, merupakan mitra Dinas Tenaga Kerja dan Transmigrasi.</p>
            <a class="btn btn-default btn-xl js-scroll-trigger" href="#services">Get Started!</a>
          </div>
        </div>
      </div>
    </section>
    <!-- <awal untuk alumni mencari data diri> -->
    <div class="call-to-action bg-dark">
      <div class="container text-center">
        <h2>KAMU ALUMNI SMKN 1 CIJULANG?</h2>
        <a class="btn btn-default btn-xl sr-button" href="<?php echo base_url()."tb_alumni" ?>">Cek Disini!</a>
      </div>
    </div>

    <!-- <awal service> -->
    <?php $this->load->view('front/f_service') ?>

    <!-- <untuk katalog perusahaan> -->
    <?php $this->load->view('front/f_peru') ?>

    <div id="lowongan" class="call-to-action bg-dark">
      <div class="container"> 
          <div class="container"> 
            <div class="row"> 
              <div class="col-lg-12 text-center"> 
                <h2>Lowongan yang tersedia!</h2> 
                <table class="table table-hover text-left"> 
                  <thead> 
                    <tr> 
                      <th>#</th> 
                      <th>Perusahaan</th> 
                      <th>Lokasi</th> 
                      <th>Expired</th> 
                      <th>Cek Sekarang</th> 
                    </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>PT. Adiaksa</td>
                    <td>Cibubur</td>
                    <td>32 Januari</td>
                    <td><a href="#">Lihat</a></td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>PT. Adiaksa</td>
                    <td>Cibubur</td>
                    <td>32 Januari</td>
                    <td><a href="#">Lihat</a></td>
                  </tr>    
                  <tr>
                    <td>1</td>
                    <td>PT. Adiaksa</td>
                    <td>Cibubur</td>
                    <td>32 Januari</td>
                    <td><a href="#">Lihat</a></td>
                  </tr>    
                  <tr>
                    <td>1</td>
                    <td>PT. Adiaksa</td>
                    <td>Cibubur</td>
                    <td>32 Januari</td>
                    <td><a href="#">Lihat</a></td>
                  </tr>    
                  <tr>
                    <td>1</td>
                    <td>PT. Adiaksa</td>
                    <td>Cibubur</td>
                    <td>32 Januari</td>
                    <td><a href="#">Lihat</a></td>
                  </tr>    
                  <tr>
                    <td>1</td>
                    <td>PT. Adiaksa</td>
                    <td>Cibubur</td>
                    <td>32 Januari</td>
                    <td><a href="#">Lihat</a></td>
                  </tr>                  
                </tbody>
                </table>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
      </div>
    </div>

    <!-- <footer ada disini nich> -->
    <?php $this->load->view('front/f_footer') ?>

    <!-- <div class="call-to-action bg-dark">
      <div class="container">
            <p>Copyright &copy; 2018 All rights reserved | Design by <a href="http://w3layouts.com">Bootstrap</a> | Edited by <a href="#">Nugraha</a></p>
        </div>
    </div>
 -->
    <!-- Bootstrap core JavaScript -->
    <?php $this->load->view('front/f_script') ?>

  </body>
</html>
