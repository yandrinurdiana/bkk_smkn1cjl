<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>asetad/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>


<ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGASI SISTEM</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tv"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('d_alumni');?>"><i class="fa fa-circle-o"></i> Data Alumni</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Data Perusahaan</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Data User</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Data Lowongan Kerja</a></li>
          </ul>
        </li>
        
        <li>
          <a href="<?php echo base_url();?>">
            <i class="fa fa-cloud-upload"></i> <span>Upload Surat</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pencil"></i> <span>Tulis Artikel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-photo"></i> <span>Gallery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
          <li>
          <a href="<?php echo base_url();?>">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        </li>        
      </ul>

      </section>
    <!-- /.sidebar -->
  </aside>