<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $judul; ?>
        <small>Version 1.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-home"></i>  <?php echo $judul; ?></a></li>
        <li class="active"> <?php echo $sub_judul; ?></li>
      </ol>
    </section>

    <div class="alert alert-block alert-success">
      <button type="button" class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>
        <i class="ace-icon green"></i>
      <strong>Sistem Aplikasi Bursa Khusus Kerja || SMKN 1 CIJULANG<small> (v1.0)</small></strong>
  </div>
<section class="content">
      <!-- Info boxes -->
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <p>Jumlah Alumni</p>
              <h3>150234</h3>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <p>Perusahaan</p>
              <h3>150234</h3>
            </div>
            <div class="icon">
              <i class="fa fa-bank"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <p>User</p>
              <h3>150234</h3>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <p>Lowongan Kerja</p>
              <h3>150234</h3>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
      </div>

      <div class="callout callout-danger">
          <h4><i class="fa fa-bullhorn"></i> Tip!</h4>

          <p>Add the layout-top-nav class to the body tag to get this layout. This feature can also be used with a
            sidebar! So use this class if you want to remove the custom dropdown menus from the navbar and use regular
            links instead.</p>
        </div>
    </section>
    </div>