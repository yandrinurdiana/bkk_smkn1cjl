<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_alumni extends CI_Model{
  function tampil_data()
  {
    $this->db->order_by("admin_user","desc"); 
    return $this->db->get('admin')->result();
  }
  function insert($data)
  {
    $this->db->insert('bkk_data_alumni', $data);
    return $this->db->insert_id();
  }
  

  public function tampil_data_alumni()
  {
    return $this->db->get('bkk_data_alumni');
  }
  public function tampil_data_admin_alumni()
  {
    
    $this->db->order_by("id_alumni","desc"); 
    return $this->db->get('bkk_data_alumni');
  }
  public function edit_alumni($where, $table)
  {
    return $this->db->get_where($table, $where);
  }
  public function update_alumni($where, $data, $table)
  {
    $this->db->where($where);
    $this->db->update($table, $data);
  }

  public function hapus_data_alumni($where, $table)
  {
    $this->db->where($where);
    $this->db->delete($table);
  }
  function fetch_data($query)
  {
    $this->db->select("*");
    $this->db->from("bkk_data_alumni");
    if($query != '')
    {
     $this->db->like('no_nis', $query);
     $this->db->or_like('nama_lengkap', $query);
     $this->db->or_like('jurusan', $query);
     $this->db->or_like('tahun_lulus', $query);
   }
   $this->db->order_by('id_alumni', 'DESC');
   return $this->db->limit(10)->get();
 }
}

?>