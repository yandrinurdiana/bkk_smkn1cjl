 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


 class M_umum extends CI_Model
 {
 	public function tampil_data()
 	{		

 		$this->db->select('*');
 		$this->db->from('bkk_data_umum');
 		$this->db->join('bkk_data_lowongan_kerja', 'bkk_data_umum.id_lk = bkk_data_lowongan_kerja.id_lk', 'INNER');
 		$this->db->where('bkk_data_umum.id_lk = bkk_data_lowongan_kerja.id_lk');
 		$this->db->order_by('id_umum','DESC');
 		return $this->db->get()->result();

 		
 	}

 	public function input_data($data, $table)
 	{

 		$this->db->insert($table, $data);
 	}

 	public function update_data($where, $data, $table)
 	{
 		$this->db->where($where);
 		$this->db->update($table, $data);
 	}
 }