<?php


class M_berita extends CI_Model
{

    public function input_berita($data, $table)
    {
        $this->db->insert($table, $data);
    }

        public function tampil_data_berita_admin()
    {
       $this->db->order_by("created_at","desc"); 
        return $this->db->get('bkk_berita')->result();
   }

    function tampil_data_berita($number,$offset){
        return $query = $this->db->order_by("created_at","desc")->get('bkk_berita',$number,$offset)->result();       
    }
    function jumlah_berita(){
        return $this->db->get('bkk_berita')->num_rows();
    }

   public function readmore($where, $table)
    {
        return $this->db->get_where($table, $where);
    }
   public function update_berita($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }



     public function update_content($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function edit_berita($where, $table)
    {
        return $this->db->get_where($table, $where);
    }
    public function hapus_data_berita($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}