<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model{

	function count_alumni(){


		return $this->db->count_all('bkk_data_alumni');
	}
	function count_loker(){
		
		
		return $this->db->count_all('bkk_data_lowongan_kerja');
	}
	function count_siswa(){
		
		
		return $this->db->count_all('bkk_siswa_user_login');
	}
	function count_perusahaan(){
		
		
		return $this->db->count_all('bkk_perusahaan_user_login');
	}

	public function tampil_data_admin_perusahaan()
    {
        
        $this->db->order_by("id_log_ph","desc"); 
		return $this->db->get('bkk_perusahaan_user_login');
    }
}