<?php


class M_perusahaan extends CI_Model
{
    public function tampil_data()
    {
        return $this->db->get('bkk_data_gaji');
    }

    public function input_data($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function hapus_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //data loker
    public function tampil_data_loker()
    {
        return $this->db->order_by('id_lk','desc')->get('bkk_data_lowongan_kerja');
    }
    public function tampil_data_loker_admin()
    {
        
        $this->db->select('*, bkk_perusahaan_user_login.nama_perusahaan as perusahaan');
    $this->db->from('bkk_perusahaan_user_login');
    $this->db->join('bkk_data_lowongan_kerja', 'bkk_data_lowongan_kerja.id_perusahaan = bkk_perusahaan_user_login.id_log_ph');
    $this->db->order_by('bkk_data_lowongan_kerja.id_lk','desc');
    $query = $this->db->get();


    return $query;
    }

    public function input_data_loker($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function edit_data_loker($where, $table)
    {
        return $this->db->get_where($table, $where);
    }
    public function edit_data_perusahaan($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function update_data_perusahaan($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function update_data_loker($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function hapus_data_loker($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
     public function hapus_data_perusahaan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
     function insert_reg($data)
    {
        $this->db->insert('bkk_perusahaan_user_login', $data);
        return $this->db->insert_id();
    }
    function tampil_data_loker_front(){
       return $this->db->limit(6)->order_by('id_lk','desc')->get('bkk_data_lowongan_kerja');     
    }
    function jumlah_loker(){
        return $this->db->get('bkk_data_lowongan_kerja')->num_rows();
    }
}
