<?php


class M_file extends CI_Model
{

    public function upload_file($data, $table)
    {
        $this->db->insert($table, $data);
    }
    public function upload_logo($data, $table)
    {
        $this->db->insert($table, $data);
    }
     public function tampil_data_file()
   {
		$this->db->order_by("created","desc"); 
		return $this->db->get('bkk_files')->result();
   }
   public function tampil_logo()
   {
        $this->db->order_by("created_at","asc"); 
        return $this->db->get('bkk_logo')->result();
   }
   public function tampil_logo_front()
   {
        $this->db->order_by("created_at","asc"); 
        return $this->db->get('bkk_logo')->result();
   }

   function getRows($params = array()){
        $this->db->select('*');
        $this->db->from('bkk_files');
        $this->db->where('status','1');
        $this->db->order_by('created','desc');
        if(array_key_exists('id_file',$params) && !empty($params['id_file'])){
            $this->db->where('id_file',$params['id_file']);
            //get records
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            //get records
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        //return fetched data
        return $result;
    }
    function getRowsLogo($params = array()){
        $this->db->select('*');
        $this->db->from('bkk_logo');
        $this->db->where('status','1');
        $this->db->order_by('created_at','desc');
        if(array_key_exists('id_logo',$params) && !empty($params['id_logo'])){
            $this->db->where('id_logo',$params['id_logo']);
            //get records
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            //get records
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        //return fetched data
        return $result;
    }

    public function update_logo($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function hapus_data_file($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
     public function hapus_data_logo($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}