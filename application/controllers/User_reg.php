<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User_reg extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('M_reg');
	}

	function sukses(){
		$this->load->view('login/f_succes');
		

	}

	function user_register()
	{
		$this->form_validation->set_rules('username', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email Address', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if($this->form_validation->run())
		{
			
			
			$data = array(
				'nama'  => $this->input->post('username'),
				'email'  => $this->input->post('email'),
				'pass' => md5($this->input->post('password')),
				'level' => '3',
				
			);
			
			$id = $this->M_reg->insert($data);
			

			if($id > 0)
			{
						$this->sukses();


			}
			else
			{
			//$this->load->view('login/f_login');
				echo "gagal";
			}
		}
	}

	

}



