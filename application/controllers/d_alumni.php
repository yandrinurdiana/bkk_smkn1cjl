<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class D_alumni extends CI_Controller {

	public function index()
	{
		$isi['judul']           ='Master';
		$isi['sub_judul']       ='Data Alumni';

		$this->load->view('admin/v_data_alumni');
		$this->load->view('admin/v_sidebar');
		
	}
}
