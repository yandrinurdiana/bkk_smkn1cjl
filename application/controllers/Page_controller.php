<?php

class Page_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //validasi jika user belum login
        $this->load->model('M_admin');
        if ($this->session->userdata('masuk') != true) {
            $url = base_url();
            redirect($url);
        }
    }

    public function index()
    {
        $this->load->view('v_dashboard');
    }

    public function auth_perusahaan()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '1') {
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Perusahaan';
            $this->load->view('perusahaan/layout', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }

    public function auth_admin()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '2') {
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Data';
            $isi['alumni'] = $this->M_admin->count_alumni();
            $isi['loker'] = $this->M_admin->count_loker();
            $isi['siswa'] = $this->M_admin->count_siswa();
            $isi['perusahaan'] = $this->M_admin->count_perusahaan();
            $this->load->view('admin/v_admin', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }

    public function auth_alumni()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '3') {
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Alumni';
            $this->load->view('alumni/v_layout', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }
}
