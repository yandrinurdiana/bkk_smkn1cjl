<?php

class Page_perushaan_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_perusahaan');
        $this->load->helper('url');
        //validasi jika user belum login
        if ($this->session->userdata('masuk') != true) {
            $url = base_url();
            redirect($url);
        }
    }
    public function index()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '1') {
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Perusahaan';
            $this->load->view('perusahaan/layout', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }
    public function data_gaji()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '1') {
            $isi['siswa'] = $this->m_perusahaan->tampil_data()->result();
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Perusahaan';
            $this->load->view('perusahaan/v_data_gaji', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }

    public function input_gaji()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '1') {
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Perusahaan';
            $this->load->view('perusahaan/v_input_gaji', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }

    public function input_aksi()
    {
        $nama_siswa = $this->input->post('nama_siswa');
        $nama_sekolah = $this->input->post('nama_sekolah');
        $jml_gaji = $this->input->post('jumlah_gaji');
        $penilaian = $this->input->post('penilaian');
        $id_perusahaan = $this->input->post('id_perusahaan');

        $data = array(
            'nama_siswa' => $nama_siswa,
            'nama_sekolah' => $nama_sekolah,
            'jml_gaji' => $jml_gaji,
            'penilaian' => $penilaian,
            'id_perusahaan' => $id_perusahaan,
            );
        $this->m_perusahaan->input_data($data, 'bkk_data_gaji');
        redirect('page_perushaan_controller/data_gaji');
    }

    public function edit($id_gaji)
    {
        $data['judul'] = 'Home';
        $data['sub_judul'] = 'Perusahaan';
        $where = array('id_gaji' => $id_gaji);
        $data['siswa'] = $this->m_perusahaan->edit_data($where, 'bkk_data_gaji')->result();
        $this->load->view('perusahaan/v_edit_gaji', $data);
    }

    public function update()
    {
        $id_gaji = $this->input->post('id_gaji');
        $nama_siswa = $this->input->post('nama_siswa');
        $nama_sekolah = $this->input->post('nama_sekolah');
        $jml_gaji = $this->input->post('jumlah_gaji');
        $penilaian = $this->input->post('penilaian');

        $data = array(
            'nama_siswa' => $nama_siswa,
            'nama_sekolah' => $nama_sekolah,
            'jml_gaji' => $jml_gaji,
            'penilaian' => $penilaian,
        );

        $where = array(
            'id_gaji' => $id_gaji,
        );

        $this->m_perusahaan->update_data($where, $data, 'bkk_data_gaji');
        redirect('page_perushaan_controller/data_gaji');
    }

    public function hapus($id_gaji)
    {
        $where = array('id_gaji' => $id_gaji);
        $this->m_perusahaan->hapus_data($where, 'bkk_data_gaji');
        redirect('page_perushaan_controller/data_gaji');
    }

    //data loker
    public function data_loker()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '1') {
            $isi['siswa'] = $this->m_perusahaan->tampil_data_loker()->result();
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Perusahaan';
            $this->load->view('perusahaan/v_data_loker', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }

    public function input_loker()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '1') {
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Perusahaan';
            $this->load->view('perusahaan/v_input_loker', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }

    public function upload()
    {
        $config['upload_path'] = 'image_loker/';  // folder upload
           $config['allowed_types'] = 'gif|jpg|png'; // jenis file
           //$config['max_size'] = 3000;
        //$config['max_width'] = 1024;
        //$config['max_height'] = 768;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) { //sesuai dengan name pada form
            echo 'anda gagal upload';
        } else {
            $nama_perusahaan = $this->input->post('nama_perusahaan');
            $posisi_kerja = $this->input->post('posisi_kerja');
            $tanggal_kadaluarsa = $this->input->post('tanggal_kadaluarsa');
            $kontak_person = $this->input->post('kontak_person');
            $deskripsi = $this->input->post('deskripsi');
            $kuota_loker = $this->input->post('kuota_loker');
            $id_perusahaan = $this->input->post('id_perusahaan');
            $file = $this->upload->data();
            $image = $file['file_name'];

            $data = array(
                'nama_perusahaan' => $nama_perusahaan,
                'posisi_kerja' => $posisi_kerja,
                'tanggal_kadaluarsa' => $tanggal_kadaluarsa,
                'kontak_person' => $kontak_person,
                'deskripsi' => $deskripsi,
                'kuota'=>$kuota_loker,
                'id_perusahaan' => $id_perusahaan,

            'image' => $image,
            );
            $this->m_perusahaan->input_data_loker($data, 'bkk_data_lowongan_kerja');
            redirect('page_perushaan_controller/data_loker');
        }
    }

    public function edit_loker($id_lk)
    {
        $data['judul'] = 'Home';
        $data['sub_judul'] = 'Perusahaan';
        $where = array('id_lk' => $id_lk);
        $data['loker'] = $this->m_perusahaan->edit_data_loker($where, 'bkk_data_lowongan_kerja')->result();
        $this->load->view('perusahaan/v_edit_loker', $data);
    }
    public function edit_loker_admin($id_lk)
    {
        $data['judul'] = 'Home';
        $data['sub_judul'] = 'Perusahaan';
        $where = array('id_lk' => $id_lk);
        $data['loker'] = $this->m_perusahaan->edit_data_loker($where, 'bkk_data_lowongan_kerja')->result();
        $this->load->view('admin/v_edit_loker', $data);
    }
    public function edit_perusahaan_admin($id_log_ph)
    {
        $data['judul'] = 'Home';
        $data['sub_judul'] = 'Perusahaan';
        $where = array('id_log_ph' => $id_log_ph);
        $data['perusahaan'] = $this->m_perusahaan->edit_data_perusahaan($where, 'bkk_perusahaan_user_login')->result();
        $this->load->view('admin/v_edit_perusahaan', $data);
    }

    public function update_perusahaan_admin()
    {
        $id_log_ph = $this->input->post('id_log_ph');
        $nama_perusahaan = $this->input->post('nama_perusahaan');
        $telp_perusahaan = $this->input->post('telp_perusahaan');
        $alamat_perusahaan = $this->input->post('alamat_perusahaan');
        $email = $this->input->post('email');
       

       
        $data = array(
            'nama_perusahaan' => $nama_perusahaan,
            'telp_perusahaan' => $telp_perusahaan,
            'alamat_perusahaan' => $alamat_perusahaan,
            'email' => $email,
            );

        $where = array(
                'id_log_ph' => $id_log_ph,
            );

        $this->m_perusahaan->update_data_perusahaan($where, $data, 'bkk_perusahaan_user_login');

        redirect('Admin/data_perusahaan');
    }

    public function update_upload_admin()
    {
        $id_lk = $this->input->post('id_lk');
        $nama_perusahaan = $this->input->post('nama_perusahaan');
        $posisi_kerja = $this->input->post('posisi_kerja');
        $tanggal_kadaluarsa = $this->input->post('tanggal_kadaluarsa');
        $kontak_person = $this->input->post('kontak_person');
        $deskripsi = $this->input->post('deskripsi');
        $kuota_loker = $this->input->post('kuota_loker');

        if ($_FILES['image']['name'] != '') {
            $config['upload_path'] = './image_loker/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 10000;
            $config['max_width'] = 100000;
            $config['max_height'] = 7768000;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                $image = $upload_data['file_name'];
            }
        } else {
            $image = $this->input->post('old_image');
        }

        $data = array(
            'nama_perusahaan' => $nama_perusahaan,
            'posisi_kerja' => $posisi_kerja,
            'tanggal_kadaluarsa' => $tanggal_kadaluarsa,
            'kontak_person' => $kontak_person,
            'deskripsi' => $deskripsi,
            'kuota' => $kuota_loker,
            'image' => $image,
            );

        $where = array(
                'id_lk' => $id_lk,
            );

        $this->m_perusahaan->update_data_loker($where, $data, 'bkk_data_lowongan_kerja');

        redirect('Admin/data_loker');
    }

    public function update_upload()
    {
        $id_lk = $this->input->post('id_lk');
        $nama_perusahaan = $this->input->post('nama_perusahaan');
        $posisi_kerja = $this->input->post('posisi_kerja');
        $tanggal_kadaluarsa = $this->input->post('tanggal_kadaluarsa');
        $kontak_person = $this->input->post('kontak_person');
        $deskripsi = $this->input->post('deskripsi');
        $kuota_loker = $this->input->post('kuota_loker');

        if ($_FILES['image']['name'] != '') {
            $config['upload_path'] = './image_loker/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 10000;
            $config['max_width'] = 100000;
            $config['max_height'] = 7768000;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                $image = $upload_data['file_name'];
            }
        } else {
            $image = $this->input->post('old_image');
        }

        $data = array(
            'nama_perusahaan' => $nama_perusahaan,
            'posisi_kerja' => $posisi_kerja,
            'tanggal_kadaluarsa' => $tanggal_kadaluarsa,
            'kontak_person' => $kontak_person,
            'deskripsi' => $deskripsi,
            'kuota' => $kuota_loker,
            'image' => $image,
            );

        $where = array(
                'id_lk' => $id_lk,
            );

        $this->m_perusahaan->update_data_loker($where, $data, 'bkk_data_lowongan_kerja');

        redirect('page_perushaan_controller/data_loker');
    }

    public function hapus_loker($id_lk)
    {
        $where = array('id_lk' => $id_lk);
        $this->m_perusahaan->hapus_data_loker($where, 'bkk_data_lowongan_kerja');
        redirect('page_perushaan_controller/data_loker');
    }
    public function hapus_loker_admin($id_lk)
    {
        $where = array('id_lk' => $id_lk);
        $this->m_perusahaan->hapus_data_loker($where, 'bkk_data_lowongan_kerja');
        redirect('Admin/data_loker');
    }
    public function hapus_perusahaan_admin($id_lk)
    {
        $where = array('id_log_ph' => $id_log_ph);
        $this->m_perusahaan->hapus_data_perusahaan($where, 'bkk_perusahaan_user_login');
        redirect('Admin/data_perusahaan');
    }
}
