<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tb_alumni extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tb_alumni_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tb_alumni/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tb_alumni/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tb_alumni/index.html';
            $config['first_url'] = base_url() . 'tb_alumni/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tb_alumni_model->total_rows($q);
        $tb_alumni = $this->Tb_alumni_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tb_alumni_data' => $tb_alumni,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('front/f_tabel', $data);
    }

    public function read($id) 
    {
        $row = $this->Tb_alumni_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_alumni' => $row->id_alumni,
		'nama' => $row->nama,
		'jenis_kelamin' => $row->jenis_kelamin,
		'ttl' => $row->ttl,
		'alamat' => $row->alamat,
		'tahun_lulus' => $row->tahun_lulus,
		'email' => $row->email,
		'kontak' => $row->kontak,
		'pekerjaan' => $row->pekerjaan,
	    );
            $this->load->view('tb_alumni/tb_alumni_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tb_alumni'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tb_alumni/create_action'),
	    'id_alumni' => set_value('id_alumni'),
	    'nama' => set_value('nama'),
	    'jenis_kelamin' => set_value('jenis_kelamin'),
	    'ttl' => set_value('ttl'),
	    'alamat' => set_value('alamat'),
	    'tahun_lulus' => set_value('tahun_lulus'),
	    'email' => set_value('email'),
	    'kontak' => set_value('kontak'),
	    'pekerjaan' => set_value('pekerjaan'),
	);
        $this->load->view('tb_alumni/tb_alumni_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
		'ttl' => $this->input->post('ttl',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'tahun_lulus' => $this->input->post('tahun_lulus',TRUE),
		'email' => $this->input->post('email',TRUE),
		'kontak' => $this->input->post('kontak',TRUE),
		'pekerjaan' => $this->input->post('pekerjaan',TRUE),
	    );

            $this->Tb_alumni_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tb_alumni'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tb_alumni_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tb_alumni/update_action'),
		'id_alumni' => set_value('id_alumni', $row->id_alumni),
		'nama' => set_value('nama', $row->nama),
		'jenis_kelamin' => set_value('jenis_kelamin', $row->jenis_kelamin),
		'ttl' => set_value('ttl', $row->ttl),
		'alamat' => set_value('alamat', $row->alamat),
		'tahun_lulus' => set_value('tahun_lulus', $row->tahun_lulus),
		'email' => set_value('email', $row->email),
		'kontak' => set_value('kontak', $row->kontak),
		'pekerjaan' => set_value('pekerjaan', $row->pekerjaan),
	    );
            $this->load->view('tb_alumni/tb_alumni_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tb_alumni'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_alumni', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
		'ttl' => $this->input->post('ttl',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'tahun_lulus' => $this->input->post('tahun_lulus',TRUE),
		'email' => $this->input->post('email',TRUE),
		'kontak' => $this->input->post('kontak',TRUE),
		'pekerjaan' => $this->input->post('pekerjaan',TRUE),
	    );

            $this->Tb_alumni_model->update($this->input->post('id_alumni', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tb_alumni'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tb_alumni_model->get_by_id($id);

        if ($row) {
            $this->Tb_alumni_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tb_alumni'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tb_alumni'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
	$this->form_validation->set_rules('ttl', 'ttl', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('tahun_lulus', 'tahun lulus', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('kontak', 'kontak', 'trim|required');
	$this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'trim|required');

	$this->form_validation->set_rules('id_alumni', 'id_alumni', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tb_alumni.php */
/* Location: ./application/controllers/Tb_alumni.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-25 21:57:45 */
/* http://harviacode.com */