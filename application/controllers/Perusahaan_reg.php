<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Perusahaan_reg extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('M_alumni');
	}

	function sukses(){
		$this->load->view('login/f_succes');
		

	}
	function perusahaan_register()
    {
        $this->form_validation->set_rules('nama_perusahaan', 'Nama Perusahaan', 'required');
        $this->form_validation->set_rules('telp_perusahaan', 'Telpon Perusahaan', 'required');
        $this->form_validation->set_rules('alamat_perusahaan', 'Alamat Perusahaan', 'required');
        $this->form_validation->set_rules('username', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email Address', 'required|trim|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run())
        {
            
            
            $data = array(
                'nama_perusahaan'  => $this->input->post('nama_perusahaan'),
                'telp_perusahaan'  => $this->input->post('telp_perusahaan'),
                'alamat_perusahaan'  => $this->input->post('alamat_perusahaan'),
                'nama'  => $this->input->post('username'),
                'email'  => $this->input->post('email'),
                'pass' => md5($this->input->post('password')),
                'level' => '1',
                
            );
            $this->load->model('M_perusahaan');
            
            $id = $this->M_perusahaan->insert_reg($data);
            

            if($id > 0)
            {
                        $this->sukses();


            }
            else
            {
            //$this->load->view('login/f_login');
                echo "gagal";
            }
        }
    }
}