<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Alumni extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('M_alumni');
        $this->load->helper('url');
        if ($this->session->userdata('masuk') != true) {
            $url = base_url();
            redirect($url);
        }
    }

    public function index()
    {
        $isi['judul'] = 'Home';
        $isi['sub_judul'] = 'Dashboard';
        $isi['sub_sub_judul'] = 'Alumni';
        $this->load->view('alumni/v_layout', $isi);
    }

    public function loker()
    {
        $isi['judul'] = 'Home';
        $isi['sub_judul'] = 'Dashboard';
        $isi['sub_sub_judul'] = 'Lowongan Pekerjaan';
        $this->load->view('alumni/v_lokerLlist', $isi);
    }

    public function daftar()
    {
        $isi['judul'] = 'Home';
        $isi['sub_judul'] = 'Dashboard';
        $isi['sub_sub_judul'] = 'Form Pendafaran';
        $this->load->view('alumni/v_formAlumni', $isi);
    }

    public function lihat()
    {

       if ($this->session->userdata('akses') == '3') {
        $isi['alumni'] = $this->M_alumni->tampil_data_alumni()->result();
        $isi['judul'] = 'Home';
        $isi['sub_judul'] = 'Dashboard';
        $isi['sub_sub_judul'] = 'Data Alumni';
        $this->load->view('alumni/v_lihatAlumni', $isi);
    } else {
        echo 'Anda tidak berhak mengakses halaman ini';
    }
}

public function insert_alumni()
{
    $data = array(
        'no_nis'  => $this->input->post('no_nis'),
        'nama_lengkap'  => $this->input->post('nama_lengkap'),
        'alamat'  => $this->input->post('alamat'),
        'email'  => $this->input->post('email'),
        'no_hp' => $this->input->post('no_hp'),
        'pekerjaan' => $this->input->post('pekerjaan'),
        'tempat_kerja' => $this->input->post('tempat_kerja'),
        'tahun_lulus' => $this->input->post('tahun_lulus'),
        'jurusan' => $this->input->post('jurusan'),
        'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
        'pesan' => $this->input->post('pesan'),

    );  



    $id = $this->M_alumni->insert($data);


    if($id > 0)
    {
        $this->lihat();


    }
    else
    {
            //$this->load->view('login/f_login');
        echo "gagal";
    }
}
function sukses(){
    $this->load->view('login/f_succes');


}
function profil (){
    $isi['judul'] = 'Home';
    $isi['sub_judul'] = 'Dashboard';
    $isi['sub_sub_judul'] = 'Data Alumni';
    $this->load->view('alumni/v_profil', $isi);
    
}

}
