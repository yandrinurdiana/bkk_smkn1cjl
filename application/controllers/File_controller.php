<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_file');
        $this->load->helper(array('form', 'url','download'));
        //validasi jika user belum login
        if ($this->session->userdata('masuk') != true) {
            $url = base_url();
            redirect($url);
        }
    }

    function do_upload()
    {
                $config['upload_path'] = './file/'; //nama folder untuk menyimpan file
                $config['overwrite'] = 'TRUE';
                $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar';
        $config['max_size'] = '2048'; //ukuran maksimal file
        $config['max_width']  = '0';
        $config['max_height']  = '0';
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload())
        {   

           $this->load->model('M_file');
           $data = array();

           $data['file'] = $this->M_file->getRows();
           $data['judul']           ='Berkas';
           $data['sub_judul']       ='Data';
           $data['error']= $this->upload->display_errors();
            $data['message']='Upload Fail'; //pesan kesalahan
            $this->load->view('berkas/layout', $data); 

        }
        else{
            $file_name = $_FILES['userfile']['name'];
            $this->upload->do_upload($file_name);
            $yes = $this->upload->data();
            $imagename = $yes['file_name'];
            $deskripsi = $this->input->post('deskripsi');
            

            $data = array(
                'deskripsi' => $deskripsi,
                'file_name' => $imagename,


            );


            $this->M_file->upload_file($data, 'bkk_files');

            

            $this->load->model('M_file');
            $data = array();

            $data['file'] = $this->M_file->getRows();
            $data['error']= '';
            $data['judul']           ='Berkas';
            $data['sub_judul']       ='Data';
        $data['message']='Upload Success'; //pesan jika proses upload berhasil
        $this->load->view('berkas/layout', $data); 

    }
    
}

public function download($id_file){
    if(!empty($id_file)){
            //load download helper
        $this->load->helper('download');

            //get file info from database
        $fileInfo = $this->M_file->getRows(array('id_file' => $id_file));

            //file path
        $file = './file/'.$fileInfo['file_name'];

            //download file from directory
        force_download($file, NULL);
    }
}

function do_upload_logo()
{
                $config['upload_path'] = './logo_perusahaan/'; //nama folder untuk menyimpan file
                $config['overwrite'] = 'TRUE';
                $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1048'; //ukuran maksimal file
        $config['max_width']  = '0';
        $config['max_height']  = '0';
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload())
        {   

           $this->load->model('M_file');
           $data = array();

           $data['logo'] = $this->M_file->tampil_logo();
           $data['judul']           ='Berkas';
           $data['sub_judul']       ='Data';
           $data['error']= $this->upload->display_errors();
            $data['message']='Upload Fail'; //pesan kesalahan
            $this->load->view('admin/v_galeri', $data); 

        }
        else{
            $file_name = $_FILES['userfile']['name'];
            $this->upload->do_upload($file_name);
            $yes = $this->upload->data();
            $imagename = $yes['file_name'];
            $deskripsi = $this->input->post('deskripsi');

            

            $data = array(
                'nama_logo_p' => $deskripsi,
                'gambar_logo' => $imagename,

            );


            $this->M_file->upload_logo($data, 'bkk_logo');

            

            $this->load->model('M_file');
            $data = array();

            $data['logo'] = $this->M_file->tampil_logo();
            $data['error']= '';
            $data['judul']           ='Berkas';
            $data['sub_judul']       ='Data';
        $data['message']='Upload Success'; //pesan jika proses upload berhasil
        $this->load->view('admin/v_galeri', $data); 

    }
    
}

public function set_aktif($id_logo)
{
   $this->load->model('M_file');

   $data = array(

    'status' => '1',
);

   $where = array(
    'id_logo' => $id_logo,
);

   $this->M_file->update_logo($where, $data, 'bkk_logo');
   redirect('admin/galeri');
}

public function set_nonaktif($id_logo)
{

    $this->load->model('M_file');

    $data = array(

        'status' => '0',
    );

    $where = array(
        'id_logo' => $id_logo,
    );

    $this->M_file->update_logo($where, $data, 'bkk_logo');
    redirect('admin/galeri');
}

public function hapus_berkas($id_file){

   $this->db->where('id_file',$id_file);
   $query = $this->db->get('bkk_files');
   $row = $query->row();

   unlink("./file/$row->file_name");

   $this->db->delete('bkk_files', array('id_file' => $id_file));
   redirect('admin/berkas');

}
public function hapus_logo($id_logo){

   $this->db->where('id_logo',$id_logo);
   $query = $this->db->get('bkk_logo');
   $row = $query->row();

   unlink("./logo_perusahaan/$row->gambar_logo");

   $this->db->delete('bkk_logo', array('id_logo' => $id_logo));
   redirect('admin/galeri');

}
}
