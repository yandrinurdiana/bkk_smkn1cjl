<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_berita');
    $this->load->helper('url');

  }


  public function index($sort=null,$order=null)
  {

    $isi['judul']           ='Berita';
    $isi['sub_judul']       ='Data';

    $this->load->model('M_file');
    $isi['logo'] = $this->M_file->tampil_logo_front();
    $jumlah_berita = $this->M_berita->jumlah_berita();
    $this->load->library('pagination');
    $config['base_url'] = base_url().'welcome/index/'.$sort.'/'.$order;
    $config['total_rows'] = $jumlah_berita;
    $config['per_page'] = 3;
    $from = $this->uri->segment(3);
    $this->pagination->initialize($config); 

    $this->load->model('M_perusahaan');

    $isi['loker'] =  $this->M_perusahaan->tampil_data_loker_front()->result();

    $isi['berita'] =  $this->M_berita->tampil_data_berita($config['per_page'],$from);
    $this->load->view('home/layout',$isi);
  }
  function detail_loker() {
    $id = $this->uri->segment(3);
    $e = $this->db->where('id_lk', $id)->get('bkk_data_lowongan_kerja')->row();

    $kirim['id_lk'] = $e->id_lk;
    $kirim['nama_perusahaan'] = $e->nama_perusahaan;
    $kirim['posisi_kerja'] = $e->posisi_kerja;

    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($kirim));
  }
  public function readmore($id_berita)
  { 
    $where = array('id_berita' => $id_berita);
    $data['readmore'] = $this->M_berita->readmore($where, 'bkk_berita')->result();
    $this->load->view('home/blogdetails', $data);
  }

  public function login()
  {
    $this->load->view('login/f_login');
  }

  public function alumni()
  {
    $this->load->view('home/alumni');
  }
  public function search_alumni()
  {
    $this->load->view('h_alumni/search_alumni');
  }

  public function berkas()
  {   
    $this->load->model('M_file');
    $isi = array();

    $isi['file'] = $this->M_file->getRows();
    $isi['error']='';
    $isi['message']='';
    $isi['judul']           ='Berkas';
    $isi['sub_judul']       ='Data';

    $this->load->view('home/berkas',$isi);

  }
  function fetch()
  {
    $output = '';
    $query = '';
    $this->load->model('M_alumni');
    if($this->input->post('query'))
    {
     $query = $this->input->post('query');
   }
   $data = $this->M_alumni->fetch_data($query);
   $output .= '
   <table class="table">
   <tr>
   <th>NIS</th>
   <th>Nama</th>
   <th>Jurusan</th>
   <th>Tahun Lulus</th>

   </tr>
   ';
   if($this->input->post('query')){
     if($data->num_rows() > 0)
     {
       foreach($data->result() as $row)
       {
        $output .= '
        <tr>
        <td>'.$row->no_nis.'</td>
        <td>'.$row->nama_lengkap.'</td>
        <td>'.$row->jurusan.'</td>
        <td>'.$row->tahun_lulus.'</td>

        </tr>
        ';
      }
    }
    else
    {
     $output .= '<tr>
     <td colspan="5">Data Tidak Ditemukan</td>
     </tr>';
   }

 }else{
  $output .= '<tr>
  <td colspan="5">No Data Found</td>
  </tr>';
}

$output .= '</table>';
echo $output;
}

public function data_loker()
{   
  $this->load->model('M_perusahaan');
  $isi['judul'] = 'Home';
  $isi['sub_judul'] = 'Perusahaan';
  $jumlah_loker = $this->M_perusahaan->jumlah_loker();
  $this->load->library('pagination');
  $config['base_url'] = base_url().'welcome/index/';
  $config['total_rows'] = $jumlah_loker;
  $config['per_page'] = 3;
  $from = $this->uri->segment(3);
  $this->pagination->initialize($config); 
  $isi['loker'] =  $this->M_perusahaan->tampil_data_loker_front($config['per_page'],$from);
  $this->load->view('home/layout', $isi);

}

public function berita()
{
  $this->load->view('berita/v_berita');
}


}
