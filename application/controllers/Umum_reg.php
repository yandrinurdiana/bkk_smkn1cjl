<?php


class Umum_reg extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('M_umum');
        $this->load->helper('url');
	}

	
	function umum_register()
    {
        
            
            $newDate = date("Y-m-d",strtotime($this->input->post('tgl_lahir')));
            $data = array(
                'nama'  => $this->input->post('nama'),
                'no_hp'  => $this->input->post('no_hp'),
                'tgl_lahir'  => $newDate,
                'alamat'  => $this->input->post('alamat'),
                'jk'  => $this->input->post('jk'),
                'email'  => $this->input->post('email'),
                'tahun_lulus'  => $this->input->post('tahun_lulus'),
                'asal_sekolah'  => $this->input->post('asal_sekolah'),
                'usia'  => $this->input->post('usia'),
                'tinggi_badan'  => $this->input->post('tinggi_badan'),
                'jurusan'  => $this->input->post('jurusan'),
                'nilai_un_matematika'  => $this->input->post('nilai_un_matematika'),
                'nilai_un_total'  => $this->input->post('nilai_un_total'),
                'id_lk'  => $this->input->post('id_lk'),
                
            );
            
            $sql ="SELECT email FROM bkk_data_umum WHERE email='".$data['email']."'";
            $query = $this->db->query($sql);
            
           if ($query->num_rows() > 0) {
               $this->gagal_reg();
            }else{



         
            $this->load->model('M_umum');

            
            $id = $this->M_umum->input_data($data,'bkk_data_umum');
            $this->berhasil();
            
        }
        
            
       
      
    }
     function berhasil(){
        $this->load->view('home/berhasil');
       } 

    function gagal_reg(){
        $this->load->view('home/gagal');
       } 
    function umum_ok($id_umum){
        $data = array(
            
            'status' => '1',
        );

        $where = array(
            'id_umum' => $id_umum,
        );

       

        $this->M_umum->update_data($where, $data, 'bkk_data_umum');
        redirect('admin/p_kerja');
    }
}