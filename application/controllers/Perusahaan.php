<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {
    
    public function index()
    {
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Perusahaan';

        $this->load->view('perusahaan/v_home',$isi);
    }
    function profil (){
    $isi['judul'] = 'Home';
    $isi['sub_judul'] = 'Dashboard';
    $isi['sub_sub_judul'] = 'Data Alumni';
    $this->load->view('perusahaan/v_profil', $isi);
    
}
 public function v_perusahaan()
    {
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Lowongan Pekerjaan';

        $this->load->view('perusahaan/v_formPerusahaan',$isi);
    }
}
