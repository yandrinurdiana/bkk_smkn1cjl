<?php

class Login_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_login');
    }

    public function index()
    {
        $this->load->view('login/f_login');
    }

    public function auth()
    {
        $username = htmlspecialchars($this->input->post('username', true), ENT_QUOTES);
        $password = htmlspecialchars($this->input->post('password', true), ENT_QUOTES);

        $cek_perusahaan = $this->m_login->auth_perusahaan($username, $password);
        $cek_admin = $this->m_login->auth_smkn1cjl($username, $password);
        $cek_siswa = $this->m_login->auth_siswa($username, $password);

        if ($cek_perusahaan->num_rows() > 0) { //jika login sebagai dosen
            $data = $cek_perusahaan->row_array();
            $this->session->set_userdata('masuk', true);
            if ($data['level'] == '1') { //Akses admin
                $this->session->set_userdata('akses', '1');
                $this->session->set_userdata('ses_id', $data['id_log_ph']);
                $this->session->set_userdata('ses_nama', $data['nama']);
                redirect('page_controller/auth_perusahaan');
            }
        }
        if ($cek_admin->num_rows() > 0) { //jika login sebagai dosen
            $data = $cek_admin->row_array();
            $this->session->set_userdata('masuk', true);
            if ($data['level'] == '2') { //Akses admin
                $this->session->set_userdata('akses', '2');
                $this->session->set_userdata('ses_id', $data['id_log_smkn1cjl']);
                $this->session->set_userdata('ses_nama', $data['nama']);
                redirect('admin/index');
            }
        }
        if ($cek_siswa->num_rows() > 0) { //jika login sebagai dosen
            $data = $cek_siswa->row_array();
            $this->session->set_userdata('masuk', true);
            if ($data['level'] == '3') { //Akses admin
                $this->session->set_userdata('akses', '3');
                $this->session->set_userdata('ses_id', $data['id_log_siswa']);
                $this->session->set_userdata('ses_nama', $data['nama']);
                redirect('page_controller/auth_alumni');
            }
        } else {  // jika username dan password tidak ditemukan atau salah
            echo 'Username Atau Password Salah';
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $url = base_url('');
        redirect($url);
    }
}
