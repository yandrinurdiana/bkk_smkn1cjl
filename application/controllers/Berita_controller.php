<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_berita');
        $this->load->helper('url');
        //validasi jika user belum login
        if ($this->session->userdata('masuk') != true) {
            $url = base_url();
            redirect($url);
        }
    }

    function insert_berita(){

        $config['upload_path'] = 'image_berita/';  // folder upload
           $config['allowed_types'] = 'gif|jpg|png'; // jenis file
           //$config['max_size'] = 3000;
        //$config['max_width'] = 1024;
        //$config['max_height'] = 768;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) { //sesuai dengan name pada form
            echo 'anda gagal upload';
        } else {
            $judul = $this->input->post('judul');
            $isi = $this->input->post('isi');
            $status = $this->input->post('status');
            $file = $this->upload->data();
            $image = $file['file_name'];

            $data = array(
            'judul' => $judul,
            'isi' => $isi,
            'status' => $status,
            'author' => 'Admin',
            'image' => $image,
            );
            $this->M_berita->input_berita($data, 'bkk_berita');
            redirect('admin/beritaList');
        }
    }

    public function edit_berita($id_berita)
    {
        $data['judul'] = 'Home';
        $data['sub_judul'] = 'Admin';
        $where = array('id_berita' => $id_berita);
        $data['berita'] = $this->M_berita->edit_berita($where, 'bkk_berita')->result();
        $this->load->view('berita/edit', $data);
    }

    public function update_berita()
    {
        
        $id_berita = $this->input->post('id_berita');
        $judul = $this->input->post('judul');
        $isi = $this->input->post('isi');
        $status = $this->input->post('status');

        if ($_FILES['image']['name'] != "") {
            $config['upload_path'] = './image_berita/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 10000;
            $config['max_width'] = 100000;
            $config['max_height'] = 7768000;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                $image = $upload_data['file_name'];
            }
        } else {
            $image = $this->input->post('old_image');
        }

        $data = array(
            
            'judul' => $judul,
            'isi' => $isi,
            'status' => $status,
            'image' => $image,
            );

         $where = array(
            'id_berita' => $id_berita,
        );

         $this->M_berita->update_content($where, $data, 'bkk_berita');
        redirect('admin/beritaList');
    }


    public function publish_berita($id_berita)
    {
    

        $data = array(
            
            'status' => '1',
        );

        $where = array(
            'id_berita' => $id_berita,
        );

        $this->M_berita->update_berita($where, $data, 'bkk_berita');
        redirect('admin/beritaList');
    }

     public function pending_berita($id_berita)
    {
    
       
        $data = array(
            
            'status' => '0',
        );

        $where = array(
            'id_berita' => $id_berita,
        );

        $this->M_berita->update_berita($where, $data, 'bkk_berita');
        redirect('admin/beritaList');
    }
    public function hapus_berita($id_berita)
    {
        $where = array('id_berita' => $id_berita);
        $this->M_berita->hapus_data_berita($where, 'bkk_berita');
        redirect('admin/beritaList');
    }

    }