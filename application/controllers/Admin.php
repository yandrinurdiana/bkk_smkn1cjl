<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->helper('url');
        //validasi jika user belum login
        if ($this->session->userdata('masuk') != true) {
            $url = base_url();
            redirect($url);
        }
    }
    
    
    public function index()
    {
        // function ini hanya boleh diakses oleh admin dan dosen
        if ($this->session->userdata('akses') == '2') {
        	$isi['alumni'] = $this->M_admin->count_alumni();
        	$isi['loker'] = $this->M_admin->count_loker();
        	$isi['siswa'] = $this->M_admin->count_siswa();
        	$isi['perusahaan'] = $this->M_admin->count_perusahaan();
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Data';
            $this->load->view('admin/layout', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }


    public function berita()
    {
        $isi['judul']           ='Berita';
        $isi['sub_judul']       ='Data';

        $this->load->view('berita/layout',$isi);
    }

    public function beritaList()
    {
        if ($this->session->userdata('akses') == '2') {
            $this->load->model('M_berita');
            $isi['berita'] = $this->M_berita->tampil_data_berita_admin();
            $isi['judul']           ='Berita';
            $isi['sub_judul']       ='Data';

            $this->load->view('berita/listBerita',$isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }
    }
   

    public function agenda()
    {
        $isi['judul']           ='Agenda';
        $isi['sub_judul']       ='Data';

        $this->load->view('agenda/layout',$isi);
    }

    public function agendaList()
    {
        $isi['judul']           ='Agenda';
        $isi['sub_judul']       ='Data';

        $this->load->view('agenda/listAgenda',$isi);
    }

    public function berkas()
    {   
        $this->load->model('M_file');
        $isi = array();

        $isi['file'] = $this->M_file->getRows();
        $isi['error']='';
        $isi['message']='';
        $isi['judul']           ='Berkas';
        $isi['sub_judul']       ='Data';

        $this->load->view('berkas/layout',$isi);
    }

    public function alumni()
    {
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Lowongan Pekerjaan';

        $this->load->view('admin/v_alumni',$isi);
    }

    public function perusahaan()
    {
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Lowongan Pekerjaan';

        $this->load->view('admin/v_perusahaan',$isi);
    }

     public function v_perusahaan()
    {
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Lowongan Pekerjaan';

        $this->load->view('perusahaan/v_formPerusahaan',$isi);
    }


    public function user()
    {
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Lowongan Pekerjaan';

        $this->load->view('admin/v_user',$isi);
    }

    public function galeri()
    {
        $this->load->model('M_file');
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Lowongan Pekerjaan';
        $isi['error']='';
        $isi['message']='';
        $isi['logo'] = $this->M_file->tampil_logo();

        $this->load->view('admin/v_galeri',$isi);
    } 
    public function loker()
    {
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Lowongan Pekerjaan';

        $this->load->view('admin/v_loker',$isi);


    }

    public function add()
    {
        $isi['judul']           ='Home';
        $isi['sub_judul']       ='Lowongan Pekerjaan';
        $this->load->view('perusahaan/v_AddLoker',$isi);

    } 
    public function show_alumni()
    {   
        if ($this->session->userdata('akses') == '2') {
            $this->load->model('M_alumni');
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Admin';
            $isi['alumni'] = $this->M_alumni->tampil_data_admin_alumni( 'bkk_data_alumni')->result();
            $this->load->view('admin/v_alumni', $isi);
        }
    }

    public function edit_alumni($id_alumni)
    {   
        if ($this->session->userdata('akses') == '2') {
            $this->load->model('M_alumni');
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Admin';
            $where = array('id_alumni' => $id_alumni);
            $isi['alumni'] = $this->M_alumni->edit_alumni($where, 'bkk_data_alumni')->result();
            $this->load->view('admin/alumni/edit', $isi);
        }
    }
    public function update_alumni()
    {          
       $this->load->model('M_alumni');
       $id_alumni = $this->input->post('id_alumni');
       $no_nis  = $this->input->post('no_nis');
       $nama_lengkap  = $this->input->post('nama_lengkap');
       $alamat  = $this->input->post('alamat');
       $email  = $this->input->post('email');
       $no_hp = $this->input->post('no_hp');
       $pekerjaan = $this->input->post('pekerjaan');
       $tempat_kerja = $this->input->post('tempat_kerja');
       $tahun_lulus = $this->input->post('tahun_lulus');
       $jurusan = $this->input->post('jurusan');
       $pendidikan_terakhir = $this->input->post('pendidikan_terakhir');
       $pesan = $this->input->post('pesan');




       $data = array(
        'no_nis'  => $no_nis,
        'nama_lengkap'  => $nama_lengkap,
        'alamat'  => $alamat,
        'email'  => $email,
        'no_hp' => $no_hp,
        'pekerjaan' => $pekerjaan,
        'tempat_kerja' => $tempat_kerja,
        'tahun_lulus' => $tahun_lulus,
        'jurusan' => $jurusan,
        'pendidikan_terakhir' => $pendidikan_terakhir,
        'pesan' => $pesan,
    );

       $where = array(
        'id_alumni' => $id_alumni,
    );

       $this->M_alumni->update_alumni($where, $data, 'bkk_data_alumni');
       redirect('admin/show_alumni');
   }

   public function hapus_alumni($id_alumni)
   {
    $this->load->model('M_alumni');
    $where = array('id_alumni' => $id_alumni);
    $this->M_alumni->hapus_data_alumni($where, 'bkk_data_alumni');
    redirect('admin/show_alumni');
}

public function data_loker()
{   
    

    if ($this->session->userdata('akses') == '2') {
        $this->load->model('m_perusahaan');
            $isi['siswa'] = $this->m_perusahaan->tampil_data_loker_admin()->result();
            $isi['judul'] = 'Home';
            $isi['sub_judul'] = 'Perusahaan';
            $this->load->view('admin/v_loker', $isi);
        } else {
            echo 'Anda tidak berhak mengakses halaman ini';
        }

}
public function data_perusahaan()
{   
    $this->load->model('M_admin');
    $isi['perusahaan'] = $this->M_admin->tampil_data_admin_perusahaan()->result();
    $isi['judul'] = 'Home';
    $isi['sub_judul'] = 'Perusahaan';
    $this->load->view('admin/v_perusahaan', $isi);

}
public function profil (){
    $isi['judul'] = 'Home';
    $isi['sub_judul'] = 'Dashboard';
    $isi['sub_sub_judul'] = 'Data Alumni';
    $this->load->view('admin/v_profil', $isi);
    
}

public function p_kerja(){
   if ($this->session->userdata('akses') == '2') {
    $this->load->model('M_umum');
    $isi['pelamar'] = $this->M_umum->tampil_data();
    $isi['judul'] = 'Home';
    $isi['sub_judul'] = 'Dashboard';
    $isi['sub_sub_judul'] = 'Data Pencari Kerja';
    $this->load->view('admin/v_kerja', $isi);

}
}


}
