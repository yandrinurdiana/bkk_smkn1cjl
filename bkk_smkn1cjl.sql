-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2020 at 05:10 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bkk_smkn1cjl`
--

-- --------------------------------------------------------

--
-- Table structure for table `bkk_berita`
--

CREATE TABLE `bkk_berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_berita`
--

INSERT INTO `bkk_berita` (`id_berita`, `judul`, `isi`, `author`, `status`, `created_at`) VALUES
(1, 'judul edit lagi kuy lagi', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'Admin', 1, '2019-08-15 23:21:21'),
(2, 'new publish', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'Admin', 1, '2019-08-16 00:44:53'),
(3, 'Berita Terbatru 3', '<p><span style=\"color: #ff0000;\"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since</span> the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'Admin', 1, '2019-08-23 04:12:29'),
(4, 'Judul Berita 4', '<p><span style=\"color: #ffff00;\"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of</span> the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>&nbsp;</p>\r\n<ol>\r\n<li>adfadf</li>\r\n<li>sdfsfgsdf</li>\r\n</ol>', 'Admin', 1, '2019-08-23 04:13:07'),
(5, 'Tes Berita Offline', '<p>siap</p>', 'Admin', 1, '2019-08-28 00:57:28'),
(6, 'Berita Terbaru Sekali', '<p><strong>Lorem Ipsum</strong><span style=\"color: #ffff00; background-color: #000000;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'Admin', 1, '2019-08-28 01:39:40'),
(7, 'berita terbaru bulan oktober', '<p>ini adalah berita terbatas dari bulan oktober</p>\r\n<p>hanya bulan oktober</p>\r\n<p>khusus bulan oktober</p>', 'Admin', 1, '2019-10-15 12:45:51'),
(8, 'Berita Terbaru desember', '<p>beria baru</p>', 'Admin', 1, '2019-12-23 09:37:22'),
(9, 'Hasil Tes Wawancara PT Angkasa Pura II', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'<span style=\"background-color: #ff0000;\">Content here, content here\'</span>, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, <strong>sometimes by accident, sometimes on purpose (injected humour and the like).</strong></p>', 'Admin', 1, '2020-01-08 03:46:47');

-- --------------------------------------------------------

--
-- Table structure for table `bkk_data_alumni`
--

CREATE TABLE `bkk_data_alumni` (
  `id_alumni` int(11) NOT NULL,
  `no_nis` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `pekerjaan` varchar(255) NOT NULL,
  `tempat_kerja` varchar(255) NOT NULL,
  `tahun_lulus` varchar(255) NOT NULL,
  `jurusan` varchar(255) NOT NULL,
  `pendidikan_terakhir` varchar(255) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_data_alumni`
--

INSERT INTO `bkk_data_alumni` (`id_alumni`, `no_nis`, `nama_lengkap`, `alamat`, `email`, `no_hp`, `pekerjaan`, `tempat_kerja`, `tahun_lulus`, `jurusan`, `pendidikan_terakhir`, `pesan`) VALUES
(5, '0036', 'YUSUF SIDIK', 'KERTAYASA\r\n', 'yusuf@gnail.com', '83869281165', 'Wiraswasta', 'PT Olah Kayu', '2013', 'Teknik Komputer Dan Jaringan', 'SMK', 'pesan tes'),
(7, '0001', 'AAN KURNIA', 'SELASARI', 'aan@gmail.com', '083869281195', 'Wiraswasta', 'PT Indomart', '2013', 'TKJ', 'SMK', 'tes'),
(8, '0002', 'ACEP EGI F', 'desa masawah', 'acep@gmail.com', '083869281195', 'Lain-lain', 'PT Indomart', '2013', 'TKJ', 'SMK', ''),
(9, '0004', 'AI SUSY', 'CIKUBANG\r\n', 'susy@gmail.com', '083869281195', 'Wiraswasta', 'Pasar Parigi', '2014', 'TKJ', 'SMK', '');

-- --------------------------------------------------------

--
-- Table structure for table `bkk_data_gaji`
--

CREATE TABLE `bkk_data_gaji` (
  `id_gaji` int(11) NOT NULL,
  `nama_siswa` varchar(255) NOT NULL,
  `nama_sekolah` varchar(255) NOT NULL,
  `jml_gaji` varchar(255) NOT NULL,
  `penilaian` text NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_data_gaji`
--

INSERT INTO `bkk_data_gaji` (`id_gaji`, `nama_siswa`, `nama_sekolah`, `jml_gaji`, `penilaian`, `id_perusahaan`) VALUES
(8, 'Hendra Nugraha', 'SMK N 1 Cijulang Pangadnaran', '250000', 'Sanagat memuaskan', 2),
(9, 'tes lagi', 'tes lagi', '10000', 'bagus', 2),
(10, 'Hendra Nugraha', 'a', '134234', 'asd', 1),
(11, 'Aan ', 'SMKN 1 Cijulang', '250000', 'disiplin sangat', 1),
(12, 'Momon', 'SMKN 1 Karangpucung', '2.500.000', 'cukup kompeten', 2),
(13, 'Dewi', 'Kulonprogo', '2.000.000', 'Kompeten', 2);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_data_lowongan_kerja`
--

CREATE TABLE `bkk_data_lowongan_kerja` (
  `id_lk` int(11) NOT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `posisi_kerja` varchar(255) NOT NULL,
  `tanggal_kadaluarsa` varchar(255) NOT NULL,
  `kontak_person` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_data_lowongan_kerja`
--

INSERT INTO `bkk_data_lowongan_kerja` (`id_lk`, `nama_perusahaan`, `posisi_kerja`, `tanggal_kadaluarsa`, `kontak_person`, `deskripsi`, `image`, `id_perusahaan`) VALUES
(16, 'PT input 3', 'input data', '', '5854168', 'asdasd', 'redmi_6a_TAM.png', 1),
(17, 'PT input 4', 'Admin Sosial Media', '', '5854168', 'asdasf', 'CHEGUEVARA_(1).png', 1),
(18, 'Astra Jakarta', 'Operator Mesin', '', '5854168', 'Bursa Kerja Khusus (BKK) adalah sebuah lembaga yang dibentuk di Sekolah Menengah Kejuruan Negeri dan Swasta, sebagai unit pelaksana yang memberikan pelayanan dan informasi lowongan kerja, pelaksana pemasaran, penyaluran dan penempatan tenaga kerja, merupakan mitra Dinas Tenaga Kerja dan Transmigrasi. ', 'enji.JPG', 1),
(19, 'Astra Jakarta', 'Operator Mesin', '', '86546788', 'deskripsi', 'Noah-Mills-Most-Handsome-Man-20162.jpg', 1),
(20, 'Garuda', 'Admin', '2020-10-03', '838', 'ini deskripsi erbaru', 'sandiaga_uno.jpg', 1),
(22, 'PT Maju Mundur Cantik', 'Admin Media Sosial', '2020-01-16', '8386928441', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n', 'cara_melihat_spek_laptop4.png', 1),
(23, 'PT. TIRTA ALAM SEGAR (TAS)', 'Magang', '2020-03-13', '08387468766', 'Info Lowongan Kerja Program Im Jepang\r\n\r\n\r\n\r\nPersyaratan :\r\n\r\n1. Laki laki.\r\n\r\n2. Belum menikah.\r\n\r\n3. Usia minimal 18 tahun bagi SMK Teknik dan untuk SMU IPA / SMK Non Teknik minimal 19 tahun 06 bulan disertai sertifikat pelatihan kerja atau paklaring, maksimal usia 25 tahun\r\n\r\n4. TB : min 162 & BB : 50 kg ( ideal ).\r\n\r\n5. Tidak bertato / tindik.\r\n\r\n6. Tidak buta warna / minus.\r\n\r\n7. Tidak pernah patah tulang.\r\n\r\n8. Lulus Pra Fisik\r\n\r\n9. Siap mengikuti pelatihan.\r\n\r\n\r\n\r\nPENDAFTARAN SECARA ONLINE :\r\n\r\nSILAHKAN MASUK KE WEBSITE : bkk-smkn2kotabekasi.com\r\n\r\nKEMUDIAN PRINT/SCREENSHOT BUKTI PENDAFTARAN TERSEBUT DAN LANGSUNG DAFTAR ULANG DI BKK SMKN 2 KOTA BEKASI SAMPAI DENGAN QUOTA TERPENUHI.\r\n\r\n\r\n\r\nPESERTA HARAP MEMPERSIAPKAN :\r\n\r\n1. KTP\r\n\r\n2. IJAZAH DAN SKHUN SD SAMPAI SMK\r\n\r\n3. AKTE LAHIR\r\n\r\n4. KARTU KELUARGA\r\n\r\n5. FOTOCOPY KTP ORANG TUA\r\n\r\n6. SURAT KETERANGAN SEHAT DARI PUSKESMAS\r\n\r\n7. SKCK (POLRES)\r\n\r\n8.KARTU KUNING (AK1)\r\n\r\n9. MENJAGA KEBERSIHAN LINGKUNGAN DAN DILARANG MEROKOK\r\n\r\n\r\n\r\nTERIMA KASIH\r\n\r\nBkksmkn2kotabekasi', 'enji2.JPG', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_data_umum`
--

CREATE TABLE `bkk_data_umum` (
  `id_umum` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jk` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tahun_lulus` varchar(15) NOT NULL,
  `asal_sekolah` varchar(255) NOT NULL,
  `usia` varchar(15) NOT NULL,
  `tinggi_badan` varchar(11) NOT NULL,
  `jurusan` varchar(255) NOT NULL,
  `nilai_un_matematika` varchar(255) NOT NULL,
  `nilai_un_total` varchar(255) NOT NULL,
  `id_lk` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_data_umum`
--

INSERT INTO `bkk_data_umum` (`id_umum`, `nama`, `no_hp`, `tgl_lahir`, `alamat`, `jk`, `email`, `tahun_lulus`, `asal_sekolah`, `usia`, `tinggi_badan`, `jurusan`, `nilai_un_matematika`, `nilai_un_total`, `id_lk`, `status`) VALUES
(6, 'Hendra Nugraha', '083869281195', '1970-01-01', 'cijulang', 'Laki-Laki', 'hnugraha26@gmail.com', '2019', 'SMK CIjulang', '18', '166', 'TKJ', '69', '70', 17, 1),
(7, 'momon', '123456789', '1997-08-14', 'cijulang', 'Laki-Laki', 'momon@yahoo.gim', '2013', 'SMKN 1 Manunjaya', '19', '169', 'TKER', '69', '80', 18, 1),
(8, 'suryadi', '085112345678', '2007-01-24', 'pnd', 'Laki-Laki', 'susy@gmail.com', '2014', 'SMK Cikaler', '20', '155', 'Mulmed', '70', '80', 16, 1),
(9, 'mamat', '081233456789', '1999-12-02', 'pangandaran', 'Laki-Laki', 'kampret@gmail.com', '2012', 'pangandaran', '23', '177', 'tataboga', '65', '85', 16, 1),
(10, 'Hendra Nugraha', '083869281195', '1970-01-01', '', 'Laki-Laki', 'sahamikun@gmail.com', '2013', 'SMKN 1 Manunjaya', '19', '166', 'Mulmed', '70', '85', 17, 1),
(11, 'isak', '0838999999', '1997-04-12', 'parigi', 'Laki-Laki', 'ilham@gampor.com', '2017', 'SMKN 1 Cijulang', '19', '176', 'Teknik Kendaraan Ringan', '87', '78', 18, 0),
(12, 'Jeri', '838266472', '2019-02-01', 'kalendasi', 'Laki-Laki', 'jseri@gmaul.com', '2013', 'SMKN 1 Cijulang', '25', '170', 'Teknik KEndasran RIngan', '80', '90', 19, 0),
(13, 'Desi', '86586358', '1970-01-01', 'cijulang', 'Laki-Laki', 'hnugraha26@gmail.com', '2013', 'SMK CIjulang', '25', '166', 'TKJ', '80', '90', 19, 0),
(14, 'Hendra Nugraha', '083869281195', '2019-12-24', 'cijulang', 'Laki-Laki', 'sahamikun@gmail.com', '2019', 'SMK CIjulang', '18', '177', 'Teknik Kendaraan Ringan', '65', '78', 18, 0),
(15, 'Hendra Nugraha', '083869281195', '2019-12-24', 'cijulang', 'Laki-Laki', 'sahamikun@gmail.com', '2019', 'SMK CIjulang', '18', '177', 'Teknik Kendaraan Ringan', '65', '78', 18, 0),
(16, 'haryadi', '08333333333333', '2015-01-04', 'karangpucung', 'Laki-Laki', 'haryadi@g.com', '2019', 'SMK Karangpucung', '17', '179', 'Teknik kendaraan Ringan', '78', '87', 22, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_files`
--

CREATE TABLE `bkk_files` (
  `id_file` int(11) NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bkk_files`
--

INSERT INTO `bkk_files` (`id_file`, `deskripsi`, `file_name`, `created`, `modified`, `status`) VALUES
(22, 'Formulir Pendaftaran', 'AKHLAK_BERPAKAIAN1.docx', '2019-08-23 06:35:48', '2019-08-23 06:35:48', 1),
(23, 'Daftar Siswa Yang Lolos Tes', 'DAFTAR_NAMA_MURID.docx', '2019-08-23 06:37:00', '2019-08-23 06:37:00', 1),
(24, 'Batasan BKKBN', 'Batasan_dan_kriteria_MDK_BKKBN.docx', '2020-01-08 02:28:08', '2020-01-08 02:28:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_logo`
--

CREATE TABLE `bkk_logo` (
  `id_logo` int(11) NOT NULL,
  `nama_logo_p` varchar(255) NOT NULL,
  `gambar_logo` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_logo`
--

INSERT INTO `bkk_logo` (`id_logo`, `nama_logo_p`, `gambar_logo`, `status`, `created_at`) VALUES
(14, 'PT Angkasa Pura', 'angkasa.png', 1, '2019-08-23 06:05:23'),
(15, 'PT Suzuki', 'suzuki.jpg', 1, '2019-08-23 06:05:32'),
(16, 'PT Yamaha', 'yamaha.jpg', 1, '2019-08-23 06:05:41'),
(17, 'pt dharma grup', 'dharma_group.png', 1, '2019-08-23 06:08:20'),
(18, 'semen indonesia', 'Logo_Semen_Indonesia.JPG', 1, '2019-08-23 06:21:11'),
(19, 'panasonic', 'Panasonic-Logo.jpg', 1, '2019-08-23 06:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `bkk_perusahaan_user_login`
--

CREATE TABLE `bkk_perusahaan_user_login` (
  `id_log_ph` int(15) NOT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `telp_perusahaan` varchar(255) DEFAULT NULL,
  `alamat_perusahaan` varchar(255) DEFAULT NULL,
  `nama` varchar(90) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(40) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_perusahaan_user_login`
--

INSERT INTO `bkk_perusahaan_user_login` (`id_log_ph`, `nama_perusahaan`, `telp_perusahaan`, `alamat_perusahaan`, `nama`, `email`, `pass`, `level`) VALUES
(1, '', '', '', 'tes_perusahaan', '', '0ae42e110b994fd300b4b1e4f077195e', 1),
(2, '', '', '', 'tes_perusahaan_2', 'perushaan@email.com', '8fdfe85ce85e39513cf1e51a46374378', 1),
(5, 'PT Alim Bangkrut', '08382883', 'Cipaganti', 'alim', 'alim@bangkrut.com', '3ea6277babd0570c650fca3d17ec4bc5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_siswa_user_login`
--

CREATE TABLE `bkk_siswa_user_login` (
  `id_log_siswa` int(15) NOT NULL,
  `nama` varchar(90) DEFAULT NULL,
  `pass` varchar(40) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_siswa_user_login`
--

INSERT INTO `bkk_siswa_user_login` (`id_log_siswa`, `nama`, `pass`, `email`, `level`) VALUES
(1, 'tes_siswa', 'bce34133611c2d6612c65d448597a5c8', '', 3),
(18, 'admin@test.com', '5b37040e6200edb3c7f409e994076872', 'admin@test.com', 3),
(19, 'acep', '7337afaa01797ee989e3c8d9874d5b3b', 'acep@gmail.com', 3),
(20, 'aka', '6ac933301a3933c8a22ceebea7000326', 'aka@gmail.com', 3),
(21, 'susy', 'ae2518f0370729389043d0874b2f229f', 'aisusy@gmail.com', 3),
(22, 'azis', 'ece4d80d2597f084b564b95c1bf2f224', 'azis@gmail.com', 3),
(23, 'ayinur', '39584a2a18614d0d094eaf73198c5b1b', 'ayi@gmail.com', 3),
(24, 'beni', 'b94ce3c426a5ab6032624ab62a2b0b95', 'beni@gmail.com', 3),
(25, 'dede', 'b4be1c568a6dc02dcaf2849852bdb13e', 'dede@gmail.com', 3),
(26, 'deris', '101f91d42aaf0f3814a14e940757f847', 'deris@gmail.com', 3),
(27, 'ega', 'b6f6c91fba2d093099ba04f42a1d65a3', 'ega@gmail.com', 3),
(28, 'elis', 'eaba1bca7df38544439d482bb60ab916', 'elis@gnail.com', 3),
(29, 'frisma', 'e5e8356697535fbe7625ef67892071d9', 'frisma@gnail.com', 3),
(30, 'ghuran', '9d5ff135bd1f9db229fc6af8d7b92cb9', 'ghuran@gnail.com', 3),
(31, 'hasanudin', '39af50d7373ca90114ac98b7dcff866c', 'hasanudin@gnail.com', 3),
(32, 'hendra', 'a04cca766a885687e33bc6b114230ee9', 'hendra@gnail.com', 3),
(33, 'husni', '143196712ca8d8714a875522c5957a6d', 'husni@gnail.com', 3),
(34, 'ina', 'a0fb2daa33c637d078d1d276dd453ea2', 'ina@gnail.com', 3),
(35, 'inka', '674f83e119a5e3fa6a6a82c8c2793c6e', 'inka@gnail.com', 3),
(36, 'ita', '78b0fb7d034c46f13890008e6f36806b', 'ita@gnail.com', 3),
(37, 'iwan', '01ccce480c60fcdb67b54f4509ffdb56', 'iwan@gnail.com', 3),
(38, 'lukman', 'b5bbc8cf472072baffe920e4e28ee29c', 'lukman@gnail.com', 3),
(39, 'mia', '5102ecd3d47f6561de70979017b87a80', 'mia@gnail.com', 3),
(40, 'mirna', '10c248ba417154af2bcbe85b58f86446', 'mirna@gnail.com', 3),
(41, 'nina', 'f2ceea1536ac1b8fed1a167a9c8bf04d', 'nina@gnail.com', 3),
(42, 'novia', 'f75f175dc9f6e5a0e4ddc9dec255b2fd', 'novia@gnail.com', 3),
(43, 'noviy', '5df9b0590ea498d146d22c2944020006', 'noviy@gnail.com', 3),
(44, 'nurul', '6968a2c57c3a4fee8fadc79a80355e4d', 'nurul@gnail.com', 3),
(45, 'rina', '3aea9516d222934e35dd30f142fda18c', 'rina@gnail.com', 3),
(46, 'riska', 'fb059ad1c514876b15b3ec40df1acdac', 'riska@gnail.com', 3),
(47, 'sheppiky', 'a0d1f83f0ae49372a7bbfc05fb834547', 'sheppiky@gnail.com', 3),
(48, 'susanti', 'c3a217b79fe6e611ba9d4c13fdcb0742', 'susanti@gnail.com', 3),
(49, 'susanto', '5c06181e1485af4fc4051d2c5aa0caba', 'susanto@gnail.com', 3),
(50, 'vina', 'e7bb4f7ed097bd6ccefc46018fda32c8', 'vina@gnail.com', 3),
(51, 'yeni', '0fc37dce7e27a505363a2586f4483b92', 'yeni@gnail.com', 3),
(52, 'yuni', '6b9d6ba55e4f27b1eb5ab5ca05d160a4', 'yuni@gnail.com', 3),
(53, 'yusuf', 'dd2eb170076a5dec97cdbbbbff9a4405', 'yusuf@gnail.com', 3),
(54, 'jeri', 'd63e6966c704eec1885b753d5b257b3c', 'jeri@gmai.com', 3);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_smkn1cjl_user_login`
--

CREATE TABLE `bkk_smkn1cjl_user_login` (
  `id_log_smkn1cjl` int(15) NOT NULL,
  `nama` varchar(90) DEFAULT NULL,
  `pass` varchar(40) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_smkn1cjl_user_login`
--

INSERT INTO `bkk_smkn1cjl_user_login` (`id_log_smkn1cjl`, `nama`, `pass`, `level`) VALUES
(1, 'tes_smkn1cjl', '0f29c46c7f1cc763eb0061348f79693c', 2),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bkk_berita`
--
ALTER TABLE `bkk_berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `bkk_data_alumni`
--
ALTER TABLE `bkk_data_alumni`
  ADD PRIMARY KEY (`id_alumni`);

--
-- Indexes for table `bkk_data_gaji`
--
ALTER TABLE `bkk_data_gaji`
  ADD PRIMARY KEY (`id_gaji`);

--
-- Indexes for table `bkk_data_lowongan_kerja`
--
ALTER TABLE `bkk_data_lowongan_kerja`
  ADD PRIMARY KEY (`id_lk`);

--
-- Indexes for table `bkk_data_umum`
--
ALTER TABLE `bkk_data_umum`
  ADD PRIMARY KEY (`id_umum`);

--
-- Indexes for table `bkk_files`
--
ALTER TABLE `bkk_files`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `bkk_logo`
--
ALTER TABLE `bkk_logo`
  ADD PRIMARY KEY (`id_logo`);

--
-- Indexes for table `bkk_perusahaan_user_login`
--
ALTER TABLE `bkk_perusahaan_user_login`
  ADD PRIMARY KEY (`id_log_ph`);

--
-- Indexes for table `bkk_siswa_user_login`
--
ALTER TABLE `bkk_siswa_user_login`
  ADD PRIMARY KEY (`id_log_siswa`);

--
-- Indexes for table `bkk_smkn1cjl_user_login`
--
ALTER TABLE `bkk_smkn1cjl_user_login`
  ADD PRIMARY KEY (`id_log_smkn1cjl`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bkk_berita`
--
ALTER TABLE `bkk_berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bkk_data_alumni`
--
ALTER TABLE `bkk_data_alumni`
  MODIFY `id_alumni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bkk_data_gaji`
--
ALTER TABLE `bkk_data_gaji`
  MODIFY `id_gaji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `bkk_data_lowongan_kerja`
--
ALTER TABLE `bkk_data_lowongan_kerja`
  MODIFY `id_lk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `bkk_data_umum`
--
ALTER TABLE `bkk_data_umum`
  MODIFY `id_umum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `bkk_files`
--
ALTER TABLE `bkk_files`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `bkk_logo`
--
ALTER TABLE `bkk_logo`
  MODIFY `id_logo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `bkk_perusahaan_user_login`
--
ALTER TABLE `bkk_perusahaan_user_login`
  MODIFY `id_log_ph` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bkk_siswa_user_login`
--
ALTER TABLE `bkk_siswa_user_login`
  MODIFY `id_log_siswa` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `bkk_smkn1cjl_user_login`
--
ALTER TABLE `bkk_smkn1cjl_user_login`
  MODIFY `id_log_smkn1cjl` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
