-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 05, 2019 at 08:39 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bkk_smkn1cjl`
--

-- --------------------------------------------------------

--
-- Table structure for table `bkk_data_umum`
--

CREATE TABLE `bkk_data_umum` (
  `id_umum` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jk` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tahun_lulus` int(15) NOT NULL,
  `asal_sekolah` varchar(255) NOT NULL,
  `usia` int(15) NOT NULL,
  `tinggi_badan` int(11) NOT NULL,
  `jurusan` varchar(255) NOT NULL,
  `nilai_un_matematika` varchar(255) NOT NULL,
  `nilai_un_total` varchar(255) NOT NULL,
  `id_lk` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_data_umum`
--

INSERT INTO `bkk_data_umum` (`id_umum`, `nama`, `no_hp`, `tgl_lahir`, `alamat`, `jk`, `email`, `tahun_lulus`, `asal_sekolah`, `usia`, `tinggi_badan`, `jurusan`, `nilai_un_matematika`, `nilai_un_total`, `id_lk`, `status`) VALUES
(1, 'cuba', '6281249120550', '1998-02-04', 'cijulang', 'Laki-Laki', 'tes2@email.com', 2015, 'smkn pasundan', 20, 70, 'TKJ', '80', '75', 10, 1),
(2, 'cuba', '6281249120550', '1998-02-04', 'cijulang', 'Laki-Laki', 'tes2@email.com', 2015, 'smkn pasundan', 20, 70, 'TKJ', '80', '75', 10, 0),
(3, 'cuba lagi', '6281249120550', '2019-09-05', 'cijulang', 'Laki-Laki', 'tes2@email.com', 2015, 'smkn pasundan', 20, 70, 'TKR', '80', '75', 11, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bkk_data_umum`
--
ALTER TABLE `bkk_data_umum`
  ADD PRIMARY KEY (`id_umum`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bkk_data_umum`
--
ALTER TABLE `bkk_data_umum`
  MODIFY `id_umum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
