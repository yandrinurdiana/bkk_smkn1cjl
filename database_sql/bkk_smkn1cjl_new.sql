-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 18, 2019 at 12:29 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bkk_smkn1cjl`
--

-- --------------------------------------------------------

--
-- Table structure for table `bkk_berita`
--

CREATE TABLE `bkk_berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_berita`
--

INSERT INTO `bkk_berita` (`id_berita`, `judul`, `isi`, `author`, `status`, `created_at`) VALUES
(1, 'judul edit lagi kuy lagi', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'Admin', 0, '2019-08-15 23:21:21'),
(2, 'new publish', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'Admin', 1, '2019-08-16 00:44:53');

-- --------------------------------------------------------

--
-- Table structure for table `bkk_data_alumni`
--

CREATE TABLE `bkk_data_alumni` (
  `id_alumni` int(11) NOT NULL,
  `no_nis` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `pekerjaan` varchar(255) NOT NULL,
  `tempat_kerja` varchar(255) NOT NULL,
  `tahun_lulus` varchar(255) NOT NULL,
  `jurusan` varchar(255) NOT NULL,
  `pendidikan_terakhir` varchar(255) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_data_alumni`
--

INSERT INTO `bkk_data_alumni` (`id_alumni`, `no_nis`, `nama_lengkap`, `alamat`, `email`, `no_hp`, `pekerjaan`, `tempat_kerja`, `tahun_lulus`, `jurusan`, `pendidikan_terakhir`, `pesan`) VALUES
(1, '', 'tes', 'asdasdasd', 'tes@email.com', '8098080980', 'Pegawai Negeri', 'tes', '2010', 'tes', 'SMK', 'sadasdasd'),
(2, '', 'teslagi', 'asdasdasd', 'tes@email.com', '8098080980', 'Pegawai Negeri', 'tes', '2010', 'tes', 'SMK', 'sadasdasd'),
(3, '098098907', 'tes', 'tes', 'tes@email.com', '89080987', 'Pegawai Swasta', 'tes', '2009', 'tes', 'S1', 'tes'),
(4, '123213123', 'tes2', 'tes2', 'tes2@email.com', '12345', 'Guru', 'tes2', '2011', 'tes2', 'S2', 'tes2');

-- --------------------------------------------------------

--
-- Table structure for table `bkk_data_gaji`
--

CREATE TABLE `bkk_data_gaji` (
  `id_gaji` int(11) NOT NULL,
  `nama_siswa` varchar(255) NOT NULL,
  `nama_sekolah` varchar(255) NOT NULL,
  `jml_gaji` int(11) NOT NULL,
  `penilaian` text NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_data_gaji`
--

INSERT INTO `bkk_data_gaji` (`id_gaji`, `nama_siswa`, `nama_sekolah`, `jml_gaji`, `penilaian`, `id_perusahaan`) VALUES
(7, 'asdasd', 'asdasd', 3432423, 'sdadasd', 0),
(8, 'Hendra Nugraha', 'SMK N 1 Cijulang Pangadnaran', 250000, 'Sanagat memuaskan', 2),
(9, 'tes lagi', 'tes lagi', 10000, 'bagus', 2);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_data_lowongan_kerja`
--

CREATE TABLE `bkk_data_lowongan_kerja` (
  `id_lk` int(11) NOT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `posisi_kerja` varchar(255) NOT NULL,
  `tanggal_kadaluarsa` varchar(255) NOT NULL,
  `kontak_person` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_data_lowongan_kerja`
--

INSERT INTO `bkk_data_lowongan_kerja` (`id_lk`, `nama_perusahaan`, `posisi_kerja`, `tanggal_kadaluarsa`, `kontak_person`, `deskripsi`, `image`, `id_perusahaan`) VALUES
(8, 'PT Adiaksa', 'Satpam', '2019-02-22', 842, ' \r\n       \r\n      masih muda ddddd\r\n \r\n', 'Black,_White_and_Colorful_Cube_Kyobi_Games_Logo6.jpg', 0),
(9, 'bb', 'bb', '2019-02-11', 567553453, ' \r\n       \r\n      asdasd    asd\r\n \r\n      ', '201f31646e8743f5980a76d4ad05f57b5.jpg', 0),
(10, 'PT KUrangYawueen', 'Teknisi', '2020-08-04', 88464654, 'iopstram', 'ahm.jpg', 0),
(11, 'tes loker 2', 'tes loker', '2019-08-13', 1231231231, 'tes loker ', 'Screen_Shot_2019-08-06_at_10_36_31.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_files`
--

CREATE TABLE `bkk_files` (
  `id_file` int(11) NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bkk_files`
--

INSERT INTO `bkk_files` (`id_file`, `deskripsi`, `file_name`, `created`, `modified`, `status`) VALUES
(20, 'tes', 'Screen_Shot_2019-08-06_at_10_36_31.png', '2019-08-16 03:43:10', '2019-08-16 03:43:10', 1),
(21, 'tes 2', 'Screen_Shot_2019-08-06_at_10_36_311.png', '2019-08-16 03:43:29', '2019-08-16 03:43:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_logo`
--

CREATE TABLE `bkk_logo` (
  `id_logo` int(11) NOT NULL,
  `nama_logo_p` varchar(255) NOT NULL,
  `gambar_logo` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_logo`
--

INSERT INTO `bkk_logo` (`id_logo`, `nama_logo_p`, `gambar_logo`, `status`, `created_at`) VALUES
(1, 'tes logo', 'KORE-Software-Product-Icons_Blue_Sponsorship.png', 0, '2019-08-18 05:20:49'),
(2, 'tes logo 2', 'KORE-Software-Product-Icons_Blue_Sponsorship1.png', 0, '2019-08-18 05:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `bkk_perusahaan_user_login`
--

CREATE TABLE `bkk_perusahaan_user_login` (
  `id_log_ph` int(15) NOT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `telp_perusahaan` varchar(255) DEFAULT NULL,
  `alamat_perusahaan` varchar(255) DEFAULT NULL,
  `nama` varchar(90) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(40) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_perusahaan_user_login`
--

INSERT INTO `bkk_perusahaan_user_login` (`id_log_ph`, `nama_perusahaan`, `telp_perusahaan`, `alamat_perusahaan`, `nama`, `email`, `pass`, `level`) VALUES
(1, '', '', '', 'tes_perusahaan', '', '0ae42e110b994fd300b4b1e4f077195e', 1),
(2, '', '', '', 'tes_perusahaan_2', 'perushaan@email.com', '8fdfe85ce85e39513cf1e51a46374378', 1),
(3, '', '', '', 'tsp@email.com', 'tsp@email.com', 'c16ac673c8dd4c53456f5259264c9d7c', 1),
(4, 'pt tes', '0980808', 'alamat tes perusahaan', 'tespr2', 'tesperusahaan@email.com', '9387de40c4d88fa92267a4dd3810cd8a', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_siswa_user_login`
--

CREATE TABLE `bkk_siswa_user_login` (
  `id_log_siswa` int(15) NOT NULL,
  `nama` varchar(90) DEFAULT NULL,
  `pass` varchar(40) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_siswa_user_login`
--

INSERT INTO `bkk_siswa_user_login` (`id_log_siswa`, `nama`, `pass`, `email`, `level`) VALUES
(1, 'tes_siswa', 'bce34133611c2d6612c65d448597a5c8', '', 3),
(12, 'waw', '7dd87e1bac147f619208ad97426ef9df', 'yandrinurdiana@gmail.com', 3),
(13, 'okok', 'b73fdaa1fb7669da760b49600c45d9be', 'yandrsinurdiana@gmail.com', 3),
(14, 'tes1', 'fa3fb6e0dccc657b57251c97db271b05', 'yandrinurdiana@gmail.com', 3),
(15, 'tes1', 'fa3fb6e0dccc657b57251c97db271b05', 'yandrinurdiana@gmail.com', 3),
(16, 'tes1', 'fa3fb6e0dccc657b57251c97db271b05', 'yandrinurdiana@gmail.com', 3),
(17, 'sja', '2bdef5c1ab8238c2763572541e8e4242', 'yandrinurdiana@gmail.com', 3),
(18, 'admin@test.com', '5b37040e6200edb3c7f409e994076872', 'admin@test.com', 3);

-- --------------------------------------------------------

--
-- Table structure for table `bkk_smkn1cjl_user_login`
--

CREATE TABLE `bkk_smkn1cjl_user_login` (
  `id_log_smkn1cjl` int(15) NOT NULL,
  `nama` varchar(90) DEFAULT NULL,
  `pass` varchar(40) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkk_smkn1cjl_user_login`
--

INSERT INTO `bkk_smkn1cjl_user_login` (`id_log_smkn1cjl`, `nama`, `pass`, `level`) VALUES
(1, 'tes_smkn1cjl', '0f29c46c7f1cc763eb0061348f79693c', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bkk_berita`
--
ALTER TABLE `bkk_berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `bkk_data_alumni`
--
ALTER TABLE `bkk_data_alumni`
  ADD PRIMARY KEY (`id_alumni`);

--
-- Indexes for table `bkk_data_gaji`
--
ALTER TABLE `bkk_data_gaji`
  ADD PRIMARY KEY (`id_gaji`);

--
-- Indexes for table `bkk_data_lowongan_kerja`
--
ALTER TABLE `bkk_data_lowongan_kerja`
  ADD PRIMARY KEY (`id_lk`);

--
-- Indexes for table `bkk_files`
--
ALTER TABLE `bkk_files`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `bkk_logo`
--
ALTER TABLE `bkk_logo`
  ADD PRIMARY KEY (`id_logo`);

--
-- Indexes for table `bkk_perusahaan_user_login`
--
ALTER TABLE `bkk_perusahaan_user_login`
  ADD PRIMARY KEY (`id_log_ph`);

--
-- Indexes for table `bkk_siswa_user_login`
--
ALTER TABLE `bkk_siswa_user_login`
  ADD PRIMARY KEY (`id_log_siswa`);

--
-- Indexes for table `bkk_smkn1cjl_user_login`
--
ALTER TABLE `bkk_smkn1cjl_user_login`
  ADD PRIMARY KEY (`id_log_smkn1cjl`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bkk_berita`
--
ALTER TABLE `bkk_berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bkk_data_alumni`
--
ALTER TABLE `bkk_data_alumni`
  MODIFY `id_alumni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bkk_data_gaji`
--
ALTER TABLE `bkk_data_gaji`
  MODIFY `id_gaji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bkk_data_lowongan_kerja`
--
ALTER TABLE `bkk_data_lowongan_kerja`
  MODIFY `id_lk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bkk_files`
--
ALTER TABLE `bkk_files`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `bkk_logo`
--
ALTER TABLE `bkk_logo`
  MODIFY `id_logo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bkk_perusahaan_user_login`
--
ALTER TABLE `bkk_perusahaan_user_login`
  MODIFY `id_log_ph` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bkk_siswa_user_login`
--
ALTER TABLE `bkk_siswa_user_login`
  MODIFY `id_log_siswa` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `bkk_smkn1cjl_user_login`
--
ALTER TABLE `bkk_smkn1cjl_user_login`
  MODIFY `id_log_smkn1cjl` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
